export const environment = {
  production: true,
  webUrl: 'https://pharmahub20200606164220.azurewebsites.net/api',
  openConnect: 'https://pharmahub20200606164220.azurewebsites.net/api/notifyHub',
  // ravePubKey: 'FLWPUBK_TEST-06f0517453ec853f6cf248048f141df1-X',
  ravePubKey: 'FLWPUBK-b2c67775d6245c754e583181c3f8b486-X',
  raveEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/charge',
  raveValidateEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge',
  raveTokenizedEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge',
  raveSubAccountEndpoint: 'https://api.ravepay.co/v2/gpx/subaccounts',
  raveBanksEndpoint: 'https://api.ravepay.co/v2/banks',
  raveUpdatedEndpoint: 'https://api.flutterwave.com/v3/charges?type=card',
  raveStandardEndpoint: 'https://api.flutterwave.com/v3/payments',
  consultationFee: '500'



  // mockWebUrl: 'https://localhost:44370/api',
  // mockOpenConnect: 'https://localhost:44370/notifyhub'
};
