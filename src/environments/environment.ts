// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  webUrl: 'https://pharmahub20200606164220.azurewebsites.net/api',
  openConnect: 'https://pharmahub20200606164220.azurewebsites.net/notifyHub',
  // webUrl: 'https://localhost:44370/api',
  // openConnect: 'https://localhost:44370/notifyhub',

  // ravePubKey: 'FLWPUBK_TEST-06f0517453ec853f6cf248048f141df1-X',
  ravePubKey: 'FLWPUBK-b2c67775d6245c754e583181c3f8b486-X',
  raveEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/charge',
  raveValidateEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge',
  raveTokenizedEndpoint: 'https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge',
  raveSubAccountEndpoint: 'https://api.ravepay.co/v2/gpx/subaccounts',
  raveBanksEndpoint: 'https://api.ravepay.co/v2/banks',
  raveUpdatedEndpoint: 'https://api.flutterwave.com/v3/charges?type=card',
  raveStandardEndpoint: 'https://api.flutterwave.com/v3/payments',
  consultationFee: '500'


};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
