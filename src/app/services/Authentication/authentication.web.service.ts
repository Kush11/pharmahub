import { User } from './../../Model/user';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, config } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthenticateDataService } from './authentication.data.service';
import * as jwt_decode from 'jwt-decode';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { environment } from 'src/environments/environment';


@Injectable({ providedIn: 'root' })
export class AuthenticateWebService implements AuthenticateDataService {
  public webUrl: string;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public registerUrl: string;
  private loggedIn = new BehaviorSubject<boolean>(false);
  mockUrl: string;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
    this.webUrl = environment.webUrl;

    // this.mockUrl = environment.mockwebUrl;
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(email: string, password: string) {
    return this.http.post<any>(`${this.webUrl}/user/authenticate`, { email, password }).pipe(
      map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.localStorage.setItem('currentUser', JSON.stringify(user));
          this.loggedIn.next(true);
          this.currentUserSubject.next(user);
        }

        return user;
      })
    );
  }

  getAll() {
    return this.http.get<User[]>(`${this.webUrl}/user`);
  }

  getById(id: any) {
    return this.http.get<User>(`${this.webUrl}/user/${id}`);
  }

  register(user: any) {
    return this.http.post(`${this.webUrl}/user/register`, user);
  }
  update(user: User) {
    console.log('user id from service', user.userId);
    return this.http.put(`${this.webUrl}/user/${user.userId}`, user);

  }
  delete(id: any) {
    return this.http.delete(`${this.webUrl}/user/${id}`);
  }

  logout() {
    // remove user from local storage to log user out
    // this.localStorage.removeItem('currentUser');
    localStorage.clear();
    this.currentUserSubject.next(null);
  }
  decode() {
    const currentUser = JSON.parse(this.localStorage.getItem('currentUser'));
    if (!currentUser) {
      return;
    } else {
      const token = currentUser.token;
      try {
        return jwt_decode(token);
      } catch (Error) {
        return null;
      }
    }
  }
}
