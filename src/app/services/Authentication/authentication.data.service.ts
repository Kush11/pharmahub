
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../Model/user';




@Injectable()
export abstract class AuthenticateDataService {
  abstract login(email, password);
  abstract getAll(): Observable<User[]>;
  abstract register(user);
  abstract update(user);
  abstract delete(user);
  abstract logout();
  abstract getById(id): Observable<User>;
  abstract get currentUserValue();
  abstract decode();
}
