import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Markets } from 'src/app/Model/markets';
import { Stores } from 'src/app/Model/stores';
import { Customer } from 'src/app/Model/customer';
import { Order } from 'src/app/Model/order';
import { Products } from 'src/app/Model/products';
import { Doctor } from 'src/app/Model/doctor';
import { Specialization } from 'src/app/Model/specialization';
import { Appointment } from 'src/app/Model/appointment';
import { Prescription } from 'src/app/Model/prescription';
import { Patient } from 'src/app/Model/patient';



@Injectable()
export abstract class MarketsDataService {
  abstract getAllMarkets(): Observable<Markets[]>;
  abstract getMarketById(id);
  abstract updateMarket(market);
  abstract delete(market);
  abstract getAllStores(): Observable<any>;
  abstract getStoresById(id);
  abstract AddReview(review);
  abstract createStore(store);
  abstract createCustomer(customer);
  abstract getCustomers(): Observable<Customer[]>;
  abstract getStoresByUserId(id);
  abstract getProductById(id): Observable<Products>;
  abstract submitOrder(order): Observable<Order>;
  abstract updateOrders(status, id);
  abstract getHomeScreenStores(): Observable<any>;
  abstract getHomeScreenProducts(): Observable<any>;
  abstract getCustomerById(id): Observable<any>;

  abstract getAllDoctorDetails(): Observable<Doctor[]>;
  abstract getDoctorDetail(id): Observable<Doctor>;
  abstract createDoctorDetail(payload): Observable<Doctor>;
  abstract updateDoctorDetail(id, payload): Observable<Doctor>;
  abstract deleteDoctor(id): Observable<Doctor>;

  abstract createMerchantAccount(payload): Observable<any>;
  abstract getBanksLookup(): Observable<any>;


  abstract getAllDoctorSpecs(): Observable<Specialization[]>;
  abstract createDoctorSpec(payload): Observable<Specialization>;

  // abstract getDoctorSpec(id): Observable<Specialization>;
  // abstract updateDoctorSpec(id, payload): Observable<Specialization>;
  // abstract deleteDoctorSpec(id): Observable<Specialization>;


  abstract getAllAppointments(): Observable<Appointment[]>;
  abstract getDoctorAppointments(id): Observable<Appointment[]>;
  abstract createDoctorAppointment(payload): Observable<Appointment>;
  abstract updateDoctorAppointment(id, payload): Observable<Appointment>;
  abstract deleteDoctorAppointment(id): Observable<Appointment>;
  abstract getPatientAppointments(id): Observable<Appointment[]>;
  abstract getAppointment(id): Observable<Appointment>;


  abstract getAllPrescriptions(): Observable<Prescription[]>;
  abstract getPrescription(id): Observable<Prescription>;
  abstract createPrescription(payload): Observable<Prescription>;
  abstract updatePrecription(id, payload): Observable<Prescription>;
  abstract deletePrescription(id): Observable<Prescription>;

  abstract getAllPatients(): Observable<Patient[]>;
  abstract getPatient(id): Observable<Patient>;
  abstract createPatient(payload): Observable<Patient>;
  // abstract updatePrecription(id, payload): Observable<Prescription>;
  abstract deletePatient(id): Observable<Patient>;
  abstract getAllPatientsByDoctorId(doctorId): Observable<Patient[]>;
}
