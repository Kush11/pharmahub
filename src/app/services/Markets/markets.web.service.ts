import { RegisterStore } from './../../Model/registerStore';
import { Stores } from './../../Model/stores';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MarketsDataService } from './markets.data.service';
import { Markets } from 'src/app/Model/markets';
import { Review } from 'src/app/Model/review';
import { Customer } from 'src/app/Model/customer';
import { Order } from 'src/app/Model/order';
import { Products } from 'src/app/Model/products';
import { environment } from 'src/environments/environment';
import { Doctor } from 'src/app/Model/doctor';
import { Specialization } from 'src/app/Model/specialization';
import { Appointment } from 'src/app/Model/appointment';
import { tap } from 'rxjs/operators';
import { P2pSevice } from '../Shared/p2p.service';
import { Observable } from 'rxjs';
import { Prescription } from 'src/app/Model/prescription';
import { Patient } from 'src/app/Model/patient';

@Injectable({ providedIn: 'root' })
export class MarketsWebService implements MarketsDataService {


  public webUrl: string;
  public mockUrl: string;
  config: HttpHeaders;
  subAccountUrl: string;
  bankLookupUrl: string;
  country: string;


  constructor(private http: HttpClient, private p2p: P2pSevice) {
    this.webUrl = environment.webUrl;
    this.subAccountUrl = environment.raveSubAccountEndpoint;
    this.bankLookupUrl = environment.raveBanksEndpoint;
    this.country = 'NG';

    // this.mockUrl = environment.mockWebUrl;

    this.config = new HttpHeaders().set('Content-Type', 'application/json')
      .set('Accept', 'application/json');
  }
  getAllPatientsByDoctorId(doctorId: any) {
    const params = new HttpParams().set('id', `${doctorId}`);
    return this.http.get<Patient[]>(`${this.webUrl}/patients/doctor`, { headers: this.config, params: params })
  }
  getAllPatients() {
    return this.http.get<Patient[]>(`${this.webUrl}/patients`);
  }
  getPatient(id: any) {
    return this.http.get<Patient>(`${this.webUrl}/patients/${id}`);

  }
  createPatient(payload: any) {
    return this.http.post<Patient>(`${this.webUrl}/patients`, payload);
  }
  deletePatient(id: any) {
    return this.http.delete<Patient>(`${this.webUrl}/patients/${id}`);
  }

  getAllPrescriptions() {
    return this.http.get<Prescription[]>(`${this.webUrl}/prescriptions`);

  }
  getPrescription(id: any) {
    return this.http.get<Prescription>(`${this.webUrl}/prescriptions/${id}`);
  }
  createPrescription(payload: any) {
    return this.http.post<Prescription>(`${this.webUrl}/prescriptions`, payload);
  }
  updatePrecription(id: any, payload: any) {
    return this.http.put<Prescription>(`${this.webUrl}/prescriptions/${id}`, payload);
  }
  deletePrescription(id: any) {
    return this.http.delete<Prescription>(`${this.webUrl}/prescriptions/${id}`);
  }

  getAllAppointments() {
    return this.http.get<Appointment[]>(`${this.webUrl}/sessions`);
  }
  getAppointment(id: any) {
    return this.http.get<Appointment>(`${this.webUrl}/sessions/${id}`);
  }
  getDoctorAppointments(id: any) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get<Appointment[]>(`${this.webUrl}/sessions/doctor`, { headers: this.config, params: params });
  }
  getPatientAppointments(id: any) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get<Appointment[]>(`${this.webUrl}/sessions/patient`, { headers: this.config, params: params });
  }
  createDoctorAppointment(payload) {
    return this.http.post<Appointment>(`${this.webUrl}/sessions`, payload);
  }
  updateDoctorAppointment(id: any, payload: any) {
    return this.http.put<Appointment>(`${this.webUrl}/sessions/${id}`, payload);
  }
  deleteDoctorAppointment(id: any) {
    return this.http.delete<Appointment>(`${this.webUrl}/sessions/${id}`);
  }

  // doctor specialization

  getAllDoctorSpecs() {
    return this.http.get<Specialization[]>(`${this.webUrl}/specializations`);
  }

  createDoctorSpec(payload: any) {
    return this.http.post<Specialization>(`${this.webUrl}/specializations`, payload);
  }


  //doctor details
  getAllDoctorDetails() {
    return this.http.get<Doctor[]>(`${this.webUrl}/doctors`);
  }
  getDoctorDetail(id: any) {
    return this.http.get<Doctor>(`${this.webUrl}/doctors/${id}`)
      .pipe(
        tap(res => {
          this.p2p.getTurnCredentials(res.user.connectionId, res.user.connectionPass);
        })
      );
  }
  createDoctorDetail(payload: any) {
    return this.http.post<Doctor>(`${this.webUrl}/doctors`, payload);
  }
  updateDoctorDetail(id: any, payload: any) {
    return this.http.put<Doctor>(`${this.webUrl}/doctors/${id}`, payload);
  }
  deleteDoctor(id: any) {
    return this.http.delete<Doctor>(`${this.webUrl}/doctors/${id}`);
  }


  //homescreen data
  getHomeScreenStores() {
    return this.http.get<Markets[]>(`${this.webUrl}/homestores`);
  }
  getHomeScreenProducts() {
    return this.http.get<Markets[]>(`${this.webUrl}/products/homeproduct`);
  }

  // markets data
  getAllMarkets() {
    return this.http.get<Markets[]>(`${this.webUrl}/markets`);
  }

  getMarketById(id: number) {
    return this.http.get<Markets>(`${this.webUrl}/markets/${id}`);
  }

  updateMarket(market: Markets) {
    return this.http.put(`${this.webUrl}/markets/${market.id}`, market);
  }
  delete(id: AnimationPlaybackEvent) {
    return this.http.delete(`${this.webUrl}/markets/${id}`);
  }

  getAllStores() {
    return this.http.get(`${this.webUrl}/stores`);
  }
  getStoresByUserId(id: any) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get<Stores>(`${this.webUrl}/stores/users`, { headers: this.config, params: params });
  }
  getStoresById(id: any) {
    return this.http.get<Stores>(`${this.webUrl}/stores/${id}`);
  }
  AddReview(review: Review) {
    return this.http.post<Review>(`${this.webUrl}/review`, JSON.stringify(review), { headers: this.config });
  }
  createStore(store: RegisterStore) {
    return this.http.post<RegisterStore>(`${this.webUrl}/stores`, JSON.stringify(store), { headers: this.config });
  }
  createCustomer(customer: Customer) {
    return this.http.post<Customer>(`${this.webUrl}/customers`, JSON.stringify(customer), { headers: this.config });
  }
  getCustomers() {
    return this.http.get<Customer[]>(`${this.webUrl}/customers`);
  }
  getCustomerById(id: any) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get(`${this.webUrl}/customers/store`, { headers: this.config, params: params });
  }
  getProductById(id: any) {
    return this.http.get<Products>(`${this.webUrl}/products/${id}`);
  }
  submitOrder(order: Order) {
    return this.http.post<Order>(`${this.webUrl}/orders`, JSON.stringify(order), { headers: this.config });
  }
  updateOrders(status: any, id: any) {
    return this.http.put(`${this.webUrl}/orders/${id}`, status);
  }

  // merchant accounts

  createMerchantAccount(payload) {
    return this.http.post(`${this.subAccountUrl}/create`, payload);
  }

  getBanksLookup() {
    const parameter = { public_key: environment.ravePubKey };
    return this.http.get<any>(`${this.bankLookupUrl}/${this.country}`, { params: parameter });
  }
}
