import { PaymentDataService } from './payment.data.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserPaymentToken, EncryptedPayment, ValidatePayment, Payment } from 'src/app/Model/payment';

@Injectable({ providedIn: 'root' })

export class PaymentWebService implements PaymentDataService {



  public webUrl: string;
  public raveUrl: string;
  validateUrl: string;
  tokenizedUrl: string;
  secKey: any;
  constructor(private http: HttpClient) {
    this.webUrl = environment.webUrl;
    this.raveUrl = environment.raveStandardEndpoint;
    this.validateUrl = environment.raveValidateEndpoint;
    this.tokenizedUrl = environment.raveTokenizedEndpoint;
  }

  // verify(verifyPayment: any) {
  //     return this.http.post<VerifyPayment>(`${this.webUrl}/payment/verify`, verifyPayment);
  // }
  create(payment: any) {
    return this.http.post<UserPaymentToken>(`${this.webUrl}/payment`, payment);
  }
  getAllPayments() {
    return this.http.get<UserPaymentToken[]>(`${this.webUrl}/payment`);
  }
  getPaymentById(paymentId: any) {
    return this.http.get<UserPaymentToken>(`${this.webUrl}/payment/${paymentId}`);

  }
  updatePayment(paymentId: any, payment: any) {
    return this.http.put<UserPaymentToken>(`${this.webUrl}/payment/${paymentId}`, payment);
  }
  delete(paymentId: any) {
    return this.http.delete<UserPaymentToken>(`${this.webUrl}/payment/${paymentId}`);
  }

  getEncryptKey() {
    return this.http.get<Payment>(`${this.webUrl}/encrypt/getKey`);
  }
  getSecKey() {
    return this.http.get<any>(`${this.webUrl}/encrypt/seckey`);
  }
  EncryptPaymentPayload(key: string, payment: any) {
    return this.http.post<any>(`${this.webUrl}/encrypt/${key}`, payment);
  }

  makePayment(encryptedPayload: any, secKey: any) {
    const options = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${secKey}`
      })
    };
    const parameter = { use_polling: '1' };
    return this.http.post<EncryptedPayment>(`${this.raveUrl}`, encryptedPayload);

  }

  standarPay(payload) {

    return this.http.post<any>(`${this.raveUrl}`, payload);
  }

  validatePayment(payload: any) {
    const parameter = { use_polling: '1' };
    return this.http.post<ValidatePayment>(`${this.validateUrl}`, payload);
  }

  redirect() {
    return this.http.get<any>(`${this.webUrl}/redirect`);
  }
  tokenizedPayment(payload: any) {
    const parameter = { use_polling: '1' };
    return this.http.post<any>(`${this.tokenizedUrl}`, payload);
  }
}
