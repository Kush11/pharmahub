import { UserPaymentToken, Payment } from 'src/app/Model/payment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()

export abstract class PaymentDataService {
  abstract create(payment): Observable<UserPaymentToken>;
  abstract getAllPayments(): Observable<UserPaymentToken[]>;
  abstract getPaymentById(paymentId): Observable<UserPaymentToken>;
  abstract getSecKey(): Observable<any>;
  abstract updatePayment(paymentId, payment): Observable<UserPaymentToken>;
  abstract delete(paymentId): Observable<UserPaymentToken>;
  abstract getEncryptKey(): Observable<Payment>;
  abstract EncryptPaymentPayload(key, payment): Observable<any>;
  abstract makePayment(encryptedPayload, secKey): Observable<any>;
  abstract validatePayment(payload): Observable<any>;
  abstract redirect(): Observable<any>;
  abstract tokenizedPayment(payload): Observable<any>;
  abstract standarPay(payload): Observable<any>;
}
