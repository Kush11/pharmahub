import { Injectable } from '@angular/core';
import { Router, NavigationEnd, UrlSerializer, ActivatedRoute } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';


@Injectable()
export class StoreService {
  public store: any;
  private previousUrl = 'undefined';
  private currentUrl = 'undefined';
  storeUrl: string;
  marketData: any;
  createDate: Date;
  markets: any[] = [];
  routeId: number;
  private _featuredProducts = new BehaviorSubject(null);
  private _filterSearch = new BehaviorSubject(null);
  private _submitted = new BehaviorSubject(null);
  private _filterStores = new BehaviorSubject(null);

  public filterSearch = this._filterSearch.asObservable();
  public featuredProductsSource = this._featuredProducts.asObservable();
  public submitted = this._submitted.asObservable();
  public filterStores = this._filterStores.asObservable();



  constructor(public router: Router, private serializer: UrlSerializer, private adminService: AdminDataService) {

    this.currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      }
    });

  }

  getPreviousUrl() {
    return this.previousUrl;
  }

  getFeaturedProducts(products) {
    this._featuredProducts.next(products);
  }

  createUrl(market: string, marketId: number) {
    const mockDomain = 'http://localhost:4200';
    const domain = 'https://marketnaija.ng';
    const newMarket = market.replace(/\s/g, '-');
    const tree = this.router.createUrlTree([], {queryParams: {store: newMarket, marketId: marketId}});
    const url  = this.serializer.serialize(tree);
    this.storeUrl = mockDomain + url;
   }

   getFilterParameters(filter) {
    this._filterSearch.next(filter);
   }

   getsubmittedStatus(submitted) {
    this._submitted.next(submitted);
   }
  
}
