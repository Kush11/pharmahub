
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Carts } from 'src/app/Model/cart';



@Injectable()
export class CartService {

cartItems = Array<Carts>();
  newItems: { 'Product': string; 'Quantity': number; 'Price': number; };

constructor() {
  this.cartItems = [
    {Product : 'Apple', Quantity: 6, Price : 1200}
  ];
}


removeCart(index) {
  this.cartItems.splice(index, 1);
}

addItems() {
  const carts = new Carts();
  this.newItems = {
    'Product': carts.Product,
    'Quantity': carts.Quantity,
    'Price': carts.Price
  };

  this.cartItems.push(this.newItems);
  console.log('cart items', this.cartItems);
}

}


