import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Howl, Howler } from 'howler';
import { P2pSevice } from './p2p.service';


@Injectable()
export class UtilityService {

  private _showHeader = new BehaviorSubject(null);
  public showHeader = this._showHeader.asObservable();

  private _showFooter = new BehaviorSubject(null);
  public showFooter = this._showFooter.asObservable();

  private _imageUrl = new BehaviorSubject(null);
  public imageUrl = this._imageUrl.asObservable();

  private _showAdminHeader = new BehaviorSubject(null);
  public showAdminHeader = this._showAdminHeader.asObservable();

  private _showModal = new BehaviorSubject(null);
  public showModal = this._showModal.asObservable();

  private _openVideo = new BehaviorSubject(null);
  public openVideo = this._openVideo.asObservable();

  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;

  uploadRef: AngularFireStorageReference;
  task: AngularFireUploadTask;
  sound: Howl;


  constructor(private afStorage: AngularFireStorage,
  ) { }

  toggleHeader(showHeader) {
    this._showHeader.next(showHeader);
  }

  toggleFooter(showFooter) {
    this._showFooter.next(showFooter);
  }

  toggleAdminHeader(showHeader) {
    this._showAdminHeader.next(showHeader);
  }

  toggleModal(show) {
    this._showModal.next(show);
  }

  toggleVideo(video) {
    this._openVideo.next(video);
  }


  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.uploadRef = this.afStorage.ref(id);
    this.task = this.uploadRef.put(event.target.files[0]);
    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        const downloadURL = this.uploadRef.getDownloadURL();
        if (downloadURL) {
          downloadURL.subscribe(res => {
            this._imageUrl.next(res);
            console.log(res);
          });
        }
      })
    ).subscribe();
  }

  playAudio() {
    this.sound = new Howl({
      src: ['../../assets/custom/ringtones/ring1.mp3'],
      autoplay: true,
      loop: false,
      volume: 0.5,
    });
  }

  playRingBack() {
    this.sound = new Howl({
      src: ['../../assets/custom/ringtones/ringback.aac'],
      autoplay: true,
      loop: false,
      volume: 0.5,
    });
  }

  stopAudio() {
    this.sound.stop();
  }
}


