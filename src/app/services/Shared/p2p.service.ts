import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { Instance } from 'simple-peer';
import { BehaviorSubject, Subject } from 'rxjs';
import { User } from 'src/app/Model/user';
import { UtilityService } from './utility.service';

declare var SimplePeer: any;

export interface PeerData {
  id: string;
  data: any;
}


@Injectable()
export class P2pSevice {
  hubConnection: any;
  webUrl = environment.openConnect;

  public currentPeer: Instance;

  private users = new BehaviorSubject(null);
  public users$ = this.users.asObservable();

  private signal = new BehaviorSubject(null);
  public signal$ = this.signal.asObservable();

  private peer = new BehaviorSubject(null);
  public peer$ = this.peer.asObservable();

  private onSignalToSend = new Subject<PeerData>();
  public onSignalToSend$ = this.onSignalToSend.asObservable();

  private onStream = new Subject<PeerData>();
  public onStream$ = this.onStream.asObservable();

  private onConnect = new Subject<PeerData>();
  public onConnect$ = this.onConnect.asObservable();

  private onData = new Subject<PeerData>();
  public onData$ = this.onData.asObservable();

  private callObject = new Subject<any>();
  public callObject$ = this.callObject.asObservable();

  private onCall = new Subject<boolean>();
  public onCall$ = this.onCall.asObservable();

  private patientSignal = new Subject<any>();
  public patientSignal$ = this.patientSignal.asObservable();

  private onEndSession = new Subject<any>();
  public onEndSession$ = this.onEndSession.asObservable();

  user: any;
  turnPassword: any;
  turnUserName: any;


  constructor(private utilityService: UtilityService) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));

  }



  async intiateConnection(groupname) {
    const loginToken = this.user.token;
    const loggedInUser = {
      groupname: groupname,
      id: this.user.id,
      username: this.user.username,
    };
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${this.webUrl}`, { accessTokenFactory: () => loginToken })
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this.hubConnection
      .start()
      .catch(err => console.error(err.toString()))
      .then(() => {
        this.hubConnection.invoke('AddToGroup', groupname)
          .then(user => {
            // this.sendSignal(groupname, JSON.stringify(loggedInUser));
          });
      });

    await this.hubConnection.on('ReceiveGroupMessage', message => {
    });

    await this.hubConnection.on('ReceiveSignal', message => {
      this.receiveSignal(message);
    });

    await this.hubConnection.on('SendSignal', signal => {
      this.patientSignal.next(signal);
    });

    await this.hubConnection.on('EndGroupCall', call => {
      this.onEndSession.next(call);
    });

    await this.hubConnection.on('CallGroupMember', message => {
      const callDetails = JSON.parse(message);
      this.callObject.next(callDetails);
    });


    this.hubConnection.onclose(() => {
      this.hubConnection.start();
    });
  }


  startCall(groupName, message) {
    this.hubConnection.invoke('SendSignal', groupName, message);
  }

  showStream(video) {
    this.utilityService.stopAudio();
    this.utilityService.toggleVideo(video);
  }

  addUserToPeer(onCall) {
    console.log(onCall);
    this.onCall.next(onCall);
  }


  rejectCall(groupName, message) {
    this.utilityService.stopAudio();
    this.hubConnection.invoke('SendSignal', groupName, message);
  }
  receiveSignal(signal) {
    const newSignal = JSON.parse(signal);
    this.signal.next(newSignal);
  }

  endGroupCall(groupName, message) {
    this.hubConnection.invoke('EndGroupCall', groupName, message);
  }

  callGroupMember(groupName, message) {
    this.hubConnection.invoke('CallGroupMember', groupName, message);
  }

  removeUserFromGroup(groupName) {
    this.hubConnection.invoke('RemoveFromGroup', groupName);
  }

  sendSignal(groupname, signal) {
    this.hubConnection.send('ReceiveSignal', groupname, signal);
  }
  sendGroupMessage(groupname, message) {
    this.hubConnection.send('ReceiveGroupMessage', groupname, message);
  }

  addNewUser(user: User) {
    this.users.next([...this.users.getValue(), user]);
  }

  disconnectUser(user: User) {
    const filteredUsers = this.users.getValue().filter(x => x.connectionId === user.connectionId);
    this.users.next(filteredUsers);
  }


  // Peer connection section

  getTurnCredentials(userName, password) {
    const config = {
      turnUserName: userName,
      turnPassword: password
    };
    localStorage.setItem('turnConfig', JSON.stringify(config));
  }
  createPeer(stream, userId: string, initiator: boolean): Instance {
    const config = JSON.parse(localStorage.getItem('turnConfig'));
    const peer = new SimplePeer({
      initiator, stream,
      config: {
        iceServers: [
          {
            url: 'stun:global.stun.twilio.com:3478?transport=udp',
            urls: 'stun:global.stun.twilio.com:3478?transport=udp'
          },
          {
            username: config.turnUserName,
            credential: config.turnPassword,
            url: 'turn:global.turn.twilio.com:3478?transport=udp',
            urls: 'turn:global.turn.twilio.com:3478?transport=udp'
          },
          {
            username: config.turnUserName,
            credential: config.turnPassword,
            url: 'turn:global.turn.twilio.com:3478?transport=tcp',
            urls: 'turn:global.turn.twilio.com:3478?transport=tcp'
          },
          {
            username: config.turnUserName,
            credential: config.turnPassword,
            url: 'turn:global.turn.twilio.com:443?transport=tcp',
            urls: 'turn:global.turn.twilio.com:443?transport=tcp'
          }
        ]
      },
    });


    peer.on('signal', data => {
      const stringData = JSON.stringify(data);
      this.onSignalToSend.next({ id: userId, data: stringData });
    });

    peer.on('stream', data => {
      this.onStream.next({ id: userId, data });
    });

    peer.on('connect', () => {
      this.onConnect.next({ id: userId, data: null });
    });

    peer.on('data', data => {
      this.onData.next({ id: userId, data });
    });
    return peer;
  }

  signalPeer(userId: string, signal: string, stream: any) {
    const signalObject = JSON.parse(signal);
    if (this.currentPeer) {
      this.currentPeer.signal(signalObject);
    } else {
      this.currentPeer = this.createPeer(stream, userId, false);
      this.currentPeer.signal(signalObject);
    }
  }

  sendMessage(message: string) {
    this.currentPeer.send(message);
  }


}
