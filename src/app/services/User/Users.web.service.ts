
import { Injectable } from '@angular/core';
import { User } from 'src/app/Model/user';
import { HttpClient } from '@angular/common/http';
import { AuthenticateUsersDataService } from './Users.data.service';
import { environment } from 'src/environments/environment';




@Injectable()
export abstract class AuthenticateUsersWebService implements AuthenticateUsersDataService {
  URL: string;
  webUrl: string;
  mockUrl: string;

  constructor(public http: HttpClient) {
    this.webUrl = environment.webUrl;

    // this.mockUrl = environment.mockWebUrl;
  }

  getAll() {
    return this.http.get<User[]>(`${this.URL}`);
  }

  getById(id: number) {
    return this.http.get<User>(`${this.URL}/${id}`);
  }

  register(user: User) {
    return this.http.post(`${this.URL}/register`, user);
  }
  update(user: User) {
    return this.http.put(`${this.URL}/${user.userId}`, user);
  }
  delete(id: number) {
    return this.http.delete(`${this.URL}/${id}`);
  }

}
