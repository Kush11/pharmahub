import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoChatComponent } from '../components/video-chat/video-chat.component';
import { CallModalComponent } from '../components/shared/modals/call-modal/call-modal.component';
import { RatingModalComponent } from '../components/shared/modals/rating-modal/rating-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [VideoChatComponent, CallModalComponent, RatingModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,


  ],
  exports: [VideoChatComponent, CallModalComponent, RatingModalComponent]
})
export class SharedModule { }
