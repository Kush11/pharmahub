import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/Model/user';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterStoreComponent } from '../register-store/register-store.component';
import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { StoreService } from 'src/app/services/Shared/store.service';
import { OrderPipe } from 'ngx-order-pipe';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { Doctor } from 'src/app/Model/doctor';

@Component({
  selector: 'app-homescreen',
  templateUrl: './homescreen.component.html',
  styleUrls: ['./homescreen.component.css']
})
export class HomescreenComponent implements OnInit, OnDestroy {
  users: User[] = [];
  private unsubscribe$ = new Subject<void>();
  productsAds: any;
  newPrice = [];
  storeAds: any;
  storeNewPrice = [];
  DiscountAds: any;
  newDiscountPrice = [];
  productPromo: any;
  banner: any;
  banners: any;
  searchText: string;
  allMarkets: any;
  allProducts: any;
  allStores: any;
  loading: boolean;
  productLoading: boolean;
  discountLoading: boolean;
  allDoctors: Doctor[];


  constructor(private router: Router, private modalService: NgbModal,
    private userService: AuthenticateDataService,
    private storeService: AdminDataService,
    private homeService: MarketsDataService,
    private orderPipe: OrderPipe,
    private sharedService: StoreService,
    private utility: UtilityService) {
    this.utility.toggleHeader(true);
    this.utility.toggleFooter(true);

  }

  ngOnInit() {
    // this.getHero();
    this.getAllDoctors()
    this.getPromotionProducts();
    this.getPromotionStores();
    this.getFeaturedDiscounts();
    this.getFilterParameters();
    this.getAllMarkets();
    this.getAllProducts();
  }

  async getFilterParameters() {

    this.sharedService.filterSearch
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.searchText = data;
        const el = document.getElementsByClassName('heading')[0];
        el.scrollIntoView({ block: 'start', behavior: 'smooth' });
      });
  }

  getAllMarkets() {
    this.loading = true;
    this.homeService.getHomeScreenStores()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {

        this.loading = false;
        this.allStores = data.filter(cs => cs.confirmStatus === true);
        console.log(data);
      },
        error => {
          this.loading = false;
          // alert('An error occured, please try again later');
        });
  }

  getAllDoctors() {
    this.loading = true;
    this.homeService.getAllDoctorDetails()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          console.log(res);
          this.allDoctors = res;
        }
      });
  }
  getAllProducts() {
    this.productLoading = true;
    this.homeService.getHomeScreenProducts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.productLoading = false;
        this.allProducts = data;
      },
        error => {
          this.productLoading = false;
          // alert('An error occured, please try again later');
        });
  }
  getHero() {
    this.storeService.getHero()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.banners = data[0];
      });
  }

  markets() {
    this.router.navigate(['markets']);
  }
  aboutUs() {
    this.router.navigate(['about']);
  }


  openFormModal() {
    const modalRef = this.modalService.open(RegisterStoreComponent);

    modalRef.result.then((result) => {

    }).catch((error) => {
      console.log(error);
    });
  }

  getPromotionProducts() {

    const currentDate = new Date();
    this.storeService.getProductAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.productsAds = data;

        this.productPromo = this.productsAds.filter(s => s.discount === false);
        this.productPromo.forEach(res => {
          const products = res.products;

          const endDate = new Date(res.endDate);
          if (currentDate > endDate && products) {

            this.storeService.deleteProductAds(res.adsId)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(ads => {
              });
          }
          this.storeService.getProductAds()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(product => {
              this.productsAds = product;
              const price = res.products.price;
              const discount = res.products.discount;
              const newPrice = (discount / 100) * price;
              const discountPrice = price - newPrice;
              this.newPrice.push(discountPrice);
            },
              error => {
                console.error('an error occured');
              });
        });
      },
        error => {
          // alert('An error occured, please try again later');
        });
  }
  getPromotionStores() {
    const currentDate = new Date();
    this.storeService.getStoreAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.storeAds = res;
        this.storeAds.forEach(element => {
          const storeId = element.adsId;
          const endDate = new Date(element.endDate);
          if (currentDate > endDate && this.storeAds) {
            this.storeService.deleteStoreAds(storeId)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(data => {
                this.storeService.getStoreAds()
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe(response => {
                    this.storeAds = response;
                  });
              });

          }
        });
      });
  }
  getFeaturedDiscounts() {
    this.discountLoading = true;
    const currentDate = new Date();
    this.storeService.getProductAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.discountLoading = false;
        const discounts = data;
        const productId =
          this.DiscountAds = discounts.filter(dis => dis.discount === true);
        this.DiscountAds.forEach(res => {
          const endDate = new Date(res.endDate);
          if (currentDate > endDate && this.DiscountAds) {
            this.storeService.deleteDiscountAds(this.DiscountAds.adsId)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(data => {
                this.storeService.getProductAds()
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe(data => {
                  });
              });
          }
          const price = res.products.price;
          const discount = res.products.discount;
          const newPrice = (discount / 100) * price;
          const discountPrice = price - newPrice;
          this.newDiscountPrice.push(discountPrice);
        });

      },
        error => {
          this.discountLoading = false;
          // alert('An error occured, please try again later');
        });
  }
  onClickproductDetails(id) {
    this.router.navigate(['productDetails', id]);
  }
  onClickStoreDetails(id) {
    this.router.navigate(['business', id], { queryParams: { businessType: 'Merchant' } });
  }

  onClickDocDetails(id) {
    this.router.navigate(['business', id], { queryParams: { businessType: 'Doctor' } });

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
