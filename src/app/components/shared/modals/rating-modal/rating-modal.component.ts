import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

interface IRating {
  id: number;
  rating: number;
  username: string;
  commentedOn: string;
  comment: string;
}

@Component({
  selector: 'app-rating-modal',
  templateUrl: './rating-modal.component.html',
  styleUrls: ['./rating-modal.component.css']
})
export class RatingModalComponent implements OnInit {
  reviewLoading: boolean;
  showItems: IRating[];
  items: IRating[] = [
    { 'id': 0, 'rating': 0, 'comment': '', 'commentedOn': '', 'username': '' }
  ];
  ratingClicked = 0;
  itemIdRatingClicked: string;
  commentForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.required],
      DoctorId: [''],
      commentedOn: [''],
      CommentBy: [''],
      userRate: [this.ratingClicked]

    });
  }


  async onSubmit() {
    this.reviewLoading = true;


  }

  ratingComponentClick(clickObj: any) {
    const item = this.items.find(((i: any) => i.id === clickObj.itemId));
    if (!!item) {
      item.rating = clickObj.rating;
      this.ratingClicked = item.rating;
    }
  }
}
