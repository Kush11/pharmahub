import { Component, OnInit, OnDestroy } from '@angular/core';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { P2pSevice } from 'src/app/services/Shared/p2p.service';

@Component({
  selector: 'app-call-modal',
  templateUrl: './call-modal.component.html',
  styleUrls: ['./call-modal.component.css']
})
export class CallModalComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  show: boolean;
  callDetails: any;
  openVideo: boolean;
  groupName: string;
  currentDate: string;


  constructor(private utility: UtilityService, private p2p: P2pSevice) { }

  ngOnInit() {
    this.p2p.callObject$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        console.log(res);
        if (res.onCall) {
          this.utility.playAudio();
          this.callDetails = res;
          this.openModal();
        } else {
          this.closeModal();
        }
      });

  }

  openModal() {
    this.show = true;

  }

  closeModal() {
    this.show = false;
    this.utility.stopAudio();

  }

  acceptCall() {
    this.p2p.showStream(this.callDetails);
    this.closeModal();
  }
  rejectCall() {
    this.p2p.rejectCall(this.callDetails.groupName, false);
    this.closeModal();
  }




  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
