import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/Model/user';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Payment } from 'src/app/Model/payment';
import { PaymentInstance } from 'angular-rave';
import { PaymentDataService } from 'src/app/services/Payment/payment.data.service';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { environment } from 'src/environments/environment';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  paymentInstance: PaymentInstance;
  token: string;
  isCard: boolean;
  isCash: boolean;
  cardDetailsForm: FormGroup;
  PBFPubKey: string;
  currency: string;
  country: string;
  amount: string;
  tfx: string;
  redirect: string;
  userId: any;
  userName: any;
  userPhone: any;
  userEmail: any;
  cardno: string;
  expiryMonth: string;
  expiryYear: string;
  cvv: string;
  encryptedData: Payment;
  userRole: any;
  billingZip: any;
  billingaddress: any;
  billingcity: any;
  billingCountry: any;
  billingState: any;
  pin: string;
  OTP: string;
  loadAuthIframe: boolean;
  authUrl: any;
  isPin: boolean;
  isAvs: boolean;
  isGtb: boolean;
  suggestedAuth: any;
  isOtp: boolean;
  transaction_ref: any;
  paymentToken: any;
  name: any;
  paymentStatus: any;
  addCard: boolean;
  loading: boolean;
  user: User;
  isIframe: boolean;
  secKey: any;
  fee: string;
  doctorId: any;
  driverAccountId: any;

  constructor(private fb: FormBuilder,
    private paymentService: PaymentDataService,
    private authService: AuthenticateDataService,
    private router: Router) {
    this.PBFPubKey = environment.ravePubKey;
    this.currency = 'NGN';
    this.country = 'NG';
    this.amount = '10';
    this.redirect = 'http://localhost:4200/payment';
  }

  ngOnInit() {
    this.getCurrentUser();
    this.cardDetailsForm = this.fb.group({
      PBFPubKey: [''],
      cardno: [''],
      cvv: [''],
      expirymonth: [''],
      expiryyear: [''],
      currency: [''],
      country: [''],
      amount: [''],
      email: [''],
      firstname: [''],
      phonenumber: [''],
      txRef: [''],
      redirect_url: [''],
      billingzip: [''],
      billingcity: [''],
      billingaddress: [''],
      billingstate: [''],
      billingcountry: [''],
      suggested_auth: [''],
      pin: [''],
      OTP: [''],
      subaccounts: [{ 'id': '' }]
    });

    this.generateReference();


  }

  getCurrentUser() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.user = user;
    this.userEmail = user.email;
    this.userPhone = user.phoneNumber;
    this.userName = user.firstName;
  }

  initiatePayment() {
    // alert('We are currently processing your card payment, please hold on.');
    // this.notifyService.showInfoMessage('We are currently processing your card payment, please hold on.');
    this.loading = true;
    this.stringifyCardDetails();
    this.paymentService
      .getEncryptKey()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(key => {
        if (key) {
          const encryptionKey = key.encryptionKey;
          this.cardDetailsForm.patchValue({
            PBFPubKey: this.PBFPubKey,
            currency: this.currency,
            country: this.country,
            amount: this.amount,
            email: this.userEmail,
            firstname: this.userName,
            phonenumber: this.userPhone,
            txRef: this.tfx,
            redirect_url: this.redirect,
            cardno: this.cardno,
            expirymonth: this.expiryMonth,
            expiryyear: this.expiryYear,
            cvv: this.cvv,
            subaccounts: [{
              'id': `${this.driverAccountId}`
            }]
          });
          const payLoad = this.cardDetailsForm.value;
          console.log(payLoad);
          this.paymentService
            .EncryptPaymentPayload(encryptionKey, payLoad)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
              if (res) {
                const paymentPayload = {
                  PBFPubKey: environment.ravePubKey,
                  client: res.encryptionData,
                  alg: '3DES-24'
                };
                this.paymentService
                  .makePayment(paymentPayload, this.secKey)
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe(payment => {
                    if (payment) {
                      this.paymentAuthCheck(payment);
                      this.loading = false;
                    }
                  }, err => {
                    this.loading = false;
                    // this.notifyService.showErrorMessage(err);
                  });
              }
            });
        } else {
          return;
        }
      });

  }

  stringifyCardDetails() {
    const cardno = this.cardDetailsForm.value.cardno;
    const expirymonth = this.cardDetailsForm.value.expirymonth;
    const expiryyear = this.cardDetailsForm.value.expiryyear;
    const cvv = this.cardDetailsForm.value.cvv;

    this.cardno = JSON.stringify(cardno);
    this.expiryYear = JSON.stringify(expiryyear);
    this.cvv = JSON.stringify(cvv);
    this.expiryMonth = expirymonth;
  }

  initiateStandardPayment() {
    const payload = {
      currency: this.currency,
      country: this.country,
      amount: this.amount,
      subaccounts: [{
        'id': `${this.driverAccountId}`
      }],
      tx_ref: this.tfx,
      redirect_url: this.redirect,
      customer: {
        email: this.userEmail,
        firstname: this.userName,
        phonenumber: this.userPhone,

      }
    };

    this.paymentService.standarPay(payload)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        console.log(res);
      });
  }
  initiateAvsAuth() {
    this.loading = true;
    const billingZip = this.cardDetailsForm.value.billingzip;
    const billingaddress = this.cardDetailsForm.value.billingaddress;
    const billingcity = this.cardDetailsForm.value.billingcity;
    const billingCountry = this.cardDetailsForm.value.billingcountry;
    const billingState = this.cardDetailsForm.value.billingstate;
    this.billingZip = billingZip;
    this.billingaddress = billingaddress;
    this.billingcity = billingcity;
    this.billingCountry = billingCountry;
    this.billingState = billingState;

    this.cardDetailsForm.patchValue({
      PBFPubKey: this.PBFPubKey,
      currency: this.currency,
      country: this.country,
      amount: this.amount,
      email: this.userEmail,
      firstname: this.userName,
      phonenumber: this.userPhone,
      txRef: this.tfx,
      redirect_url: this.redirect,
      cardno: this.cardno,
      expirymonth: this.expiryMonth,
      expiryyear: this.expiryYear,
      cvv: this.cvv,
      billingzip: this.billingZip,
      billingaddress: this.billingaddress,
      billingcity: this.billingcity,
      billingcountry: this.billingCountry,
      billingState: this.billingState,
      suggested_auth: this.suggestedAuth,
    });

    this.paymentService
      .getEncryptKey()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(key => {
        if (key) {
          const payLoad = this.cardDetailsForm.value;
          this.paymentService
            .EncryptPaymentPayload(key.encryptionKey, payLoad)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
              if (res) {
                const paymentPayload = {
                  PBFPubKey: environment.ravePubKey,
                  client: res.encryptionData,
                  alg: '3DES-24'
                };
                this.paymentService
                  .makePayment(paymentPayload, this.secKey)
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe(payment => {
                    if (payment) {
                      this.loading = false;
                      const url = payment.data.authurl;
                      this.loadIFrame(url);
                    }
                  });
              }
            });
        } else {
          return;
        }
      });
  }

  initiatePinAuth() {
    this.loading = true;
    const pin = this.cardDetailsForm.value.pin;
    this.pin = JSON.stringify(pin);
    this.cardDetailsForm.patchValue({
      PBFPubKey: this.PBFPubKey,
      currency: this.currency,
      country: this.country,
      amount: this.fee,
      email: this.userEmail,
      firstname: this.userName,
      phonenumber: this.userPhone,
      txRef: this.tfx,
      redirect_url: this.redirect,
      cardno: this.cardno,
      expirymonth: this.expiryMonth,
      expiryyear: this.expiryYear,
      cvv: this.cvv,
      pin: this.pin,
      suggested_auth: this.suggestedAuth,
    });

    this.paymentService
      .getEncryptKey()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(key => {
        if (key) {
          const payLoad = this.cardDetailsForm.value;
          this.paymentService
            .EncryptPaymentPayload(key.encryptionKey, payLoad)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
              if (res) {
                const paymentPayload = {
                  PBFPubKey: environment.ravePubKey,
                  client: res.encryptionData,
                  alg: '3DES-24'
                };
                const secKey = this.secKey;
                this.paymentService
                  .makePayment(paymentPayload, secKey)
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe(payment => {
                    if (payment) {
                      this.loading = false;
                      // this.notifyService.showInfoMessage('Please enter the OTP that was sent to you.');
                      this.transaction_ref = payment.data.flwRef;
                      this.isOtp = true;
                      this.isPin = false;
                    }
                  }, err => {
                    this.loading = false;
                    // this.notifyService.showErrorMessage(err);
                  });
              }
            });
        } else {
          return;
        }
      });
  }

  validatePayment() {
    this.loading = true;
    const otp = this.cardDetailsForm.value.OTP;
    this.OTP = JSON.stringify(otp);
    const authPayLoad = {
      PBFPubKey: this.PBFPubKey,
      transaction_reference: this.transaction_ref,
      otp: this.OTP
    };
    this.paymentService
      .validatePayment(authPayLoad)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        if (data.status === 'success') {
          const statusMessage = data.message;
          alert(statusMessage);

          this.isCard = false;
          this.isCash = false;
          this.loading = false;
          localStorage.setItem('isPaymentDone', JSON.stringify(true));
          this.clearCardDetailsForm();
          // const paymentToken = data.data.tx.chargeToken.embed_token;
          this.router.navigate(['business', this.doctorId], { queryParams: { businessType: 'Doctor', paymentDone: true } });


          // this.saveCardToken(paymentToken);
        }
      }, err => {
        this.loading = false;
        // this.notifyService.showErrorMessage(err);
      });
  }


  paymentAuthCheck(body) {
    if (
      body.status === 'success' &&
      body.data.suggested_auth === 'NOAUTH_INTERNATIONAL'
    ) {
      /*
      *card requires AVS authentication so you need to
      *1. Update payload with billing info - billingzip, billingcity, billingaddress, billingstate,
       billingcountry and the suggested_auth returned
      *2. Re-encrypt the payload
      *3. Call the charge endpoint once again with this updated encrypted payload
      */
      this.isOtp = false;
      this.isPin = false;
      this.isAvs = true;
      this.isIframe = false;
      this.suggestedAuth = body.data.suggested_auth;
    } else if (
      body.status === 'success' &&
      body.data.suggested_auth === 'PIN'
    ) {
      /*
       *card requires pin authentication so you need to
       *1. Update payload with pin and the suggested_auth returned
       *2. Re-encrypt the payload
       *3. Call the charge endpoint once again with this updated encrypted payload
       */
      // this.notifyService.showInfoMessage('Please enter your pin');
      this.isOtp = false;
      this.isAvs = false;
      this.isPin = true;
      this.isIframe = false;
      this.suggestedAuth = body.data.suggested_auth;
    } else if (
      body.status === 'success' &&
      body.data.suggested_auth === 'GTB_OTP'
    ) {
      /*
       *card requires OTP authentication so you need to
       *1. Collect OTP from user
       *2. Call Rave Validate endpoint
       */
      // this.notifyService.showInfoMessage('Please enter the OTP that was sent to you.');
      this.isAvs = false;
      this.isPin = false;
      this.isOtp = true;
      this.isIframe = false;

    } else if (body.status === 'success' && body.data.authurl !== 'N/A') {
      /*
       *card requires 3dsecure authentication so you need to
       *1. Load the authurl in an iframe for your user to complete the transaction
       */
      this.isAvs = false;
      this.isPin = false;
      this.isOtp = false;
      this.loadIFrame(body.data.authurl);
    } else {
      // this.notifyService.showErrorMessage('An error occured, this might be due to a poor network connection, please try again.')
    }
  }

  loadIFrame(url) {
    this.isAvs = false;
    this.isPin = false;
    this.isOtp = false;
    this.isIframe = true;
    this.authUrl = url;
  }

  saveCardToken(token) {
    this.authService.getById(this.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (this.user.userPaymentData.length === 0) {
          const cardDetails = {
            userId: this.userId,
            paymentToken: token
          };
          this.paymentService
            .create(cardDetails)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
              // this.notifyService.showSuccessMessage(
              //   'Your Payment details has been saved in a secure vault.'
              // );
            });
        } else {
          this.updateCardToken(token);
        }
      });
  }

  updateCardToken(token) {
    this.authService
      .getById(this.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        // const userPaymentData = res.userPayment.paymentToken;
        // if (userPaymentData === token) {

        // }
        console.log('payment response', res);
      });
  }

  generateReference() {
    let text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 10; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
      this.tfx = text;
    }

    this.paymentService.getSecKey()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.secKey = res.encryptionKey;
        localStorage.setItem('secKey', this.secKey);
      });
    const paymentData = JSON.parse(localStorage.getItem('paymentData'));
    this.fee = paymentData.consultFee;
    this.doctorId = paymentData.doctorId;
    this.driverAccountId = paymentData.paymentAccountId;
  }
  clearCardDetailsForm() {
    this.cardDetailsForm.markAsPristine();
    this.cardDetailsForm.markAsUntouched();
    this.cardDetailsForm.reset();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
