import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { Doctor } from 'src/app/Model/doctor';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterStoreComponent } from '../register-store/register-store.component';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {
  public markets;
  emptyMarket: boolean;
  isCollapsed: boolean;
  marketImage = [];
  searchText: any;
  loadSpinner: boolean;
  doctors: Doctor[];
  emptyDoctors: boolean;

  constructor(private marketService: MarketsDataService, private router: Router, private modalService: NgbModal) {

  }

  ngOnInit() {
    this.loadSpinner = true;
    this.isCollapsed = true;
    this.marketService.getAllDoctorDetails().subscribe(doctors => {
      this.doctors = doctors;
      this.loadSpinner = false;
      if (this.doctors.length === 0) {
        this.emptyDoctors = true;
        this.loadSpinner = false;
      }
    });
  }

  getMarket(marketId) {
    this.router.navigate(['markets', marketId]);
  }

  viewDoctor(id) {
    this.router.navigate(['business', id], { queryParams: { businessType: 'Doctor' } });
  }

  openFormModal() {
    const modalRef = this.modalService.open(RegisterStoreComponent);

    modalRef.result.then((result) => {
    }).catch((error) => {
      console.log(error);
    });
  }

}
