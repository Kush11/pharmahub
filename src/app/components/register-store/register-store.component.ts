import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { first, takeUntil } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Role } from 'src/app/Model/role';
import { Subject } from 'rxjs';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { Specialization } from 'src/app/Model/specialization';
import { WorkingDays } from 'src/app/Model/working-days';
import { IDropdownSettings } from 'ng-multiselect-dropdown/multiselect.model';
import { PaymentDataService } from 'src/app/services/Payment/payment.data.service';
import { Bank } from 'src/app/Model/bank';



@Component({
  selector: 'app-register-store',
  templateUrl: './register-store.component.html',
  styleUrls: ['./register-store.component.css']
})
export class RegisterStoreComponent implements OnInit, OnDestroy {
  storeUrl: any;
  merchantForm: FormGroup;
  marketData: any;
  createDate: any;
  submitted: boolean;
  merchantRoleForm: FormGroup;
  userId: any;
  user: any;
  username: any;
  address: any;
  email: any;
  error: any;
  private unsubscribe$ = new Subject<void>();
  defaultImage: string;
  productUrl: string;
  storeId: any;
  showBusinessLocation = false;
  pic: string;
  allowReg: boolean;
  role: Role;
  imageUrl: any;
  doctorSpecs: Specialization[];
  showMerchantForm: boolean;
  showDoctorForm: boolean;
  storeType: any;
  doctorForm: FormGroup;
  selectedSpecId: any;
  workingDays: WorkingDays[] = [
    {
      dayId: '1',
      day: 'Monday'
    },
    {
      dayId: '2',
      day: 'Tuesday'
    },
    {
      dayId: '3',
      day: 'Wednesday'
    },
    {
      dayId: '4',
      day: 'Thursday'
    },
    {
      dayId: '5',
      day: 'Friday'
    },
    {
      dayId: '6',
      day: 'Saturday'
    },
    {
      dayId: '7',
      day: 'Sunday'
    }
  ];
  doctorWorkingDays = [];
  workingDaysSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'dayId',
    textField: 'day',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    allowSearchFilter: false
  };



  specializationTypeSettings = {
    singleSelection: true,
    idField: 'specializationId',
    textField: 'specializationName',
    selectAllText: '',
    unSelectAllText: '',
    closeDropDownOnSelection: true,
    allowSearchFilter: true
  };
  bankSettings = {
    singleSelection: true,
    idField: 'Code',
    textField: 'Name',
    selectAllText: '',
    unSelectAllText: '',
    closeDropDownOnSelection: true,
    allowSearchFilter: true
  };

  businessTypeData = [
    {
      id: '1',
      name: 'Doctor',
    },
    {
      id: '2',
      name: 'Pharmacy'
    }
  ];

  businessTypeSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'name',
    selectAllText: '',
    unSelectAllText: '',
    allowSearchFilter: false,
    closeDropDownOnSelection: true,
  };
  loading: boolean;
  isProfileImage: boolean;
  doctorProfilemage: any;
  doctorLicense: any;
  banks: Bank;
  secKey: any;
  driverAccountId: any;
  pharmacyAccountId: any;
  doctorAccountId: any;
  selectedBank: any;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private formBuilder: FormBuilder,
    private storeWebService: MarketsDataService,
    public activeModal: NgbActiveModal,
    private router: Router,
    private authService: AuthenticateDataService,
    private utilityService: UtilityService,
    private paymentService: PaymentDataService
  ) { }

  ngOnInit() {
    this.getUserDetails();
    this.getStoreDetails();
    this.getDoctorSpecs();
    this.createDate = new Date().toDateString();
    this.utilityService.imageUrl
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          console.log(this.isProfileImage);
          this.loading = false;
          if (!this.isProfileImage) {
            this.doctorLicense = res;
            this.doctorForm.patchValue({
              licenseUrl: res
            });
          } else {
            this.doctorProfilemage = res;
            this.merchantRoleForm.patchValue({
              imageUrl: res
            });
          }
        } else {
          this.loading = false;
          return;
        }
      });

    this.merchantForm = this.formBuilder.group({
      storeName: ['', Validators.required],
      email: ['', Validators.required],
      confirmStatus: [true, Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      // storeType: [this.storeType, Validators.required],
      rating: [1, Validators.required],
      userId: [this.userId, Validators.required],
      pharmacyBank: ['', [Validators.required]],
      accountNumber: ['', [Validators.required]],
      PaymentAccountId: ['', [Validators.required]],
    });


    this.merchantRoleForm = this.formBuilder.group({
      userId: [this.userId, Validators.required],
      role: ['', Validators.required],
      imageUrl: [this.imageUrl, Validators.required],
    });

    this.doctorForm = this.formBuilder.group({
      userId: [this.userId, Validators.required],
      specializationId: ['', Validators.required],
      licenseUrl: ['', Validators.required],
      workingDays: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      rating: ['1', Validators.required],
      status: ['Inactive', Validators.required],
      isAvaialable: [false, Validators.required],
      doctorBank: ['', [Validators.required]],
      accountNumber: ['', [Validators.required]],
      PaymentAccountId: ['', [Validators.required]],
    });
    this.getBanksLookup();
    this.getSecKey();


  }



  get f() { return this.merchantForm.controls; }

  async getStoreDetails() {
    await this.storeWebService.getAllMarkets().subscribe(markets => {
      this.marketData = markets;
    });
  }

  async registerStore() {
    this.user = JSON.parse(this.localStorage.getItem('currentUser'));
    this.merchantRoleForm.patchValue({
      userId: this.user.userId
    });
    this.merchantForm.patchValue({
      userId: this.user.userId,
    });
    await this.authService.update(this.merchantRoleForm.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        userData => {
          if (this.merchantForm.invalid) {
            alert('Please fill all fields');
            return;
          } else {
            this.storeWebService
              .createStore(this.merchantForm.value)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                store => {
                  alert('Your store has been created successfully');
                  this.closeModal();
                  this.localStorage.removeItem('currentUser');
                  location.reload();
                  alert('Please login to access you dashboard');
                  this.router.navigate(['login']);
                },
                error => {
                  alert(
                    'An error occured while creating your store. Please try again shortly.'
                  );
                }
              );
          }
        },
        error => {
          alert('An error occurred while creating your store, please try again shortly.');
        }
      );
  }

  registerDoctor() {
    this.user = JSON.parse(this.localStorage.getItem('currentUser'));
    this.merchantRoleForm.patchValue({
      userId: this.user.userId
    });
    this.doctorForm.patchValue({
      userId: this.user.userId,
      PaymentAccountId: this.doctorAccountId
    });
    this.authService.update(this.merchantRoleForm.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        userData => {
          console.log(this.doctorForm.value);
          this.storeWebService.createDoctorDetail(this.doctorForm.value)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(res => {
              alert('Your account was created successfully, Please login to access your dashboard.');
              this.localStorage.removeItem('currentUser');
              this.router.navigate(['login']);
              this.closeModal();
            }, err => {
              alert('An error occured, please try again');
            });
        }, err => {
          alert('An error occured, please try again');
        });
  }
  getStoreType(value) {
    this.storeType = value.name;
    if (value.name === 'Doctor') {
      this.showDoctorForm = true;
      this.showMerchantForm = false;
      this.role = Role.Doctor;
    } else {
      this.showDoctorForm = false;
      this.showMerchantForm = true;
      this.role = Role.Merchant;
    }
    this.merchantRoleForm.patchValue({
      role: this.role
    });
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  getUserDetails() {
    this.user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (this.user) {
      this.username = this.user.firstName;
      this.address = this.user.address;
      this.email = this.user.email;
      this.userId = this.user.userId;
    } else {
      alert('Please Login or Create an account if you do not have one');
      this.closeModal();
      this.router.navigate(['login']);
    }
  }
  uploadLicense(event, profileImage) {
    this.isProfileImage = profileImage;
    this.utilityService.upload(event);
    this.loading = true;
  }

  uploadUserImage(event, profileImage) {
    this.isProfileImage = profileImage;
    this.utilityService.upload(event);
    this.loading = true;
    console.log(this.isProfileImage);
  }

  getDoctorSpecs() {
    this.storeWebService.getAllDoctorSpecs()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.doctorSpecs = res;
      });
  }

  getSelectedSpec(value) {
    this.selectedSpecId = value.specializationId;
    this.doctorForm.patchValue({
      specializationId: this.selectedSpecId
    });
  }
  getAllSelectedDays(value) {
    this.doctorWorkingDays.splice(0);
    value.forEach(element => {
      this.doctorWorkingDays = [...this.doctorWorkingDays, element.day];
    });
    this.doctorForm.patchValue({
      workingDays: JSON.stringify(this.doctorWorkingDays)
    });
  }
  getSelectedDays(value) {
    this.doctorWorkingDays = [...this.doctorWorkingDays, value.day];
    this.doctorForm.patchValue({
      workingDays: JSON.stringify(this.doctorWorkingDays)
    });
  }
  getSelectedBank(value) {
    this.doctorForm.patchValue({
      doctorBank: value.Name,
      PaymentAccountId: value.Code
    });
    console.log(this.doctorForm.value);
  }
  getAllDeselectedDays(value) {
    this.doctorWorkingDays = value;
  }

  getDeselectedDays(value) {
    this.doctorWorkingDays = this.doctorWorkingDays.filter(x => x !== value.day);
    this.doctorForm.patchValue({
      workingDays: JSON.stringify(this.doctorWorkingDays)
    });
  }

  getBanksLookup() {
    this.storeWebService.getBanksLookup()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.banks = res.data.Banks;
        console.log(this.banks);
      });
  }

  getSecKey() {
    this.paymentService.getSecKey()
      .subscribe(res => {
        this.secKey = res.encryptionKey;
      });
  }

  createDoctorAccount() {
    const accountPayload = {
      account_bank: this.doctorForm.value.doctorBank,
      account_number: this.doctorForm.value.accountNumber,
      business_name: `Dr. ${this.user.firstName} ${this.user.lastName}`,
      business_email: this.user.email,
      business_mobile: this.user.phoneNumber,
      seckey: this.secKey,
      split_type: 'percentage',
      split_value: '0.53',
      country: 'NG'
    };

    this.storeWebService.createMerchantAccount(accountPayload)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          this.doctorAccountId = res.data.subaccount_id;
          this.registerDoctor();
        }
      });
  }

  createPharmacyAccount() {
    const accountPayload = {
      account_bank: this.merchantForm.value.PharmacyBank,
      account_number: this.merchantForm.value.accountNumber,
      business_name: this.merchantForm.value.storeName,
      business_email: this.merchantForm.value.email,
      business_mobile: this.merchantForm.value.phoneNumber,
      seckey: this.secKey,
      split_type: 'percentage',
      split_value: '0.10',
      country: 'NG'
    };

    this.storeWebService.createMerchantAccount(accountPayload)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          this.pharmacyAccountId = res.data.subaccount_id;
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
