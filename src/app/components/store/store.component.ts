import { LoginComponent } from './../login/login.component';
import { ProductDetailsComponent } from './../product-details/product-details.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreService } from './../../services/Shared/store.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { CartService } from 'src/app/services/Shared/cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { saveAs } from 'file-saver';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Role } from 'src/app/Model/role';
import { Doctor } from 'src/app/Model/doctor';
import { P2pSevice } from 'src/app/services/Shared/p2p.service';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { Appointment } from 'src/app/Model/appointment';
import { environment } from 'src/environments/environment';
import { element } from 'protractor';




@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit, OnDestroy {
  storeDetails: any;
  routeId: number;
  storeName: any;
  commentForm: FormGroup;
  customerForm: FormGroup;
  storeId: any;
  userId: any;
  store: any;
  submitted: boolean;
  loading: boolean;
  orderForm: FormGroup;
  marketId: any;
  storeReview: any;
  commentDate: string;
  userName: any;
  storeType: any;
  storeAddress: any;
  storeEmail: any;
  storePhone: any;
  private unsubscribe$ = new Subject<void>();
  userAddress: any;
  userPhone: any;
  isEmpty: boolean;
  emptyStore: boolean;
  storeDescription: any;
  checkBoxValue: any = false;

  itemReview = [];
  helperArray: Array<any> = [];
  imageToShow: any;
  reviewLoading: boolean;



  userRating: any;
  review: [];
  ratings: number[];
  newReview: any;
  reviewId: any;
  isDisabledState = true;
  products: any;
  discounts: any = [];
  imagePath: any = [];
  isImageLoading: boolean;
  mySrc: any;
  attachmentFileName: any;
  productImage: any;
  storeImage: any;
  image: any;
  productId: any;
  showSlasher = [];
  searchText: any;
  storeOwner: any;
  isRegistered: boolean;
  isDiscount = [];
  businessType: any;
  doctorDetails: Doctor;
  workDays: string;
  workTime: string;
  startTime: Date;
  endTime: Date;
  openVideo: boolean;
  groupName: string;
  openModal: boolean;
  userImgUrl: any;
  showAilment: boolean;
  showCallDialogue: boolean;
  ailmentForm: FormGroup;
  pendingAppointments: Appointment[];
  genderAdj: string;
  genderText: string;
  availMessage: string;
  patientForm: FormGroup;
  patientId: string;
  consultationFee: string;
  showModal: boolean;
  isPaymentDone: boolean;
  doctorId: string;
  modalMessage: string;
  sessionId: string;
  showPayButton: boolean;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private cartService: CartService,
    private route: ActivatedRoute,
    private storeService: MarketsDataService,
    private routeService: StoreService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private adminStore: AdminDataService,
    private p2pService: P2pSevice,
    private utility: UtilityService
  ) {
    this.getStoreSearch();
  }

  ngOnInit() {
    this.consultationFee = environment.consultationFee;
    this.localStorage.removeItem('User Rating');
    this.createReviewDate();
    this.route.params.subscribe(p => {
      this.routeId = p['id'];
    });
    this.getCurrentUser();
    // this.getPatientAppointments();

    this.route.queryParams.subscribe(res => {
      this.businessType = res.businessType;
    });

    this.route.queryParams.subscribe(res => {
      if (res.paymentDone) {
        this.updateAppointment();
      }
    });

    if (this.businessType === Role.Doctor) {
      this.getDoctorDetails();

    } else if (this.businessType === Role.Merchant) {
      this.getStores();
      this.getStoreImage();
    } else {
      return;
    }

    this.customerForm = this.formBuilder.group({
      userName: [this.userName, Validators.required],
      createdOn: [this.commentDate, Validators.required],
      storeId: [this.routeId],
      userId: [this.userId, Validators.required]
    });

    // this.patientForm = this.formBuilder.group({
    //   userId: [this.userId, Validators.required],
    //   doctorId: [this.doctorDetails.doctorId, Validators.required]
    // });

    this.ailmentForm = this.formBuilder.group({
      patientAilment: ['', Validators.required]
    });

  }

  get f() { return this.commentForm.controls; }


  addcartItem(id) {
    this.cartService.addItems();
  }



  getCurrentUser() {
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (!user) {
      this.userName = 'Anonymous';
    } else {
      this.userName = `${user.firstName} ${user.lastName}`;
      this.userId = user.userId;
      this.userAddress = user.address;
      this.userImgUrl = user.imageUrl;
    }

  }

  getStores() {
    this.storeService.getStoresById(this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(store => {
        if (store) {
          this.emptyStore = false;
          this.isEmpty = false;
          this.storeDetails = store;
          this.helperArray.splice(0);
          this.storeName = this.storeDetails['storeName'];
          this.marketId = this.storeDetails['marketId'];
          this.storeReview = this.storeDetails['review'];
          this.storePhone = this.storeDetails['phoneNumber'];
          this.storeEmail = this.storeDetails['email'];
          this.storeAddress = this.storeDetails['address'];
          this.storeType = this.storeDetails['storeType'];
          this.storeDescription = this.storeDetails['Description'];
          this.storeOwner = this.storeDetails['user'];

          // const storeImage = this.storeDetails['storeImage'];
          // this.storeImage = storeImage.file;
          this.routeService.createUrl(this.storeName, this.marketId);
          this.storeReview.map(element => {
            const review = element.userRate;
            const reviewId = element.id;
            const username = element.userName;
            const comment = element.comment;
            const commentOn = element.commentedOn;

            // this.showItems = [
            //   { 'id': reviewId, 'rating': review, 'username': username, 'commentedOn': commentOn, 'comment': comment }
            // ];
            // this.helperArray.push(this.showItems);
          },
            error => {
              this.emptyStore = true;
              this.isEmpty = true;
            });
          this.products = this.storeDetails.products;
          this.products.forEach(element => {
            const discount = element.discount;
            if (discount !== 0) {
              const isDiscount = true;
              this.productId = element.productId;
              const originalPrice = Number(element.price);
              const newPrice = (originalPrice - (discount / 100) * originalPrice);
              this.discounts.push(newPrice);
              this.isDiscount.push(isDiscount);
            } else {
              const isDiscount = false;
              const newPrice = Number(element.price);
              this.discounts.push(newPrice);
              this.isDiscount.push(isDiscount);
            }
          });
        }
      });
  }

  routeToPayment() {
    const paymentData = {
      consultFee: this.consultationFee,
      doctorId: this.doctorId,
      paymentAccountId: this.doctorDetails.paymentAccountId
    };
    localStorage.setItem('paymentData', JSON.stringify(paymentData));
    this.router.navigate(['/payment']);
  }

  getDoctorDetails() {
    this.storeService.getDoctorDetail(this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.doctorId = res.doctorId;
          if (res.user.gender === 'Male') {
            this.genderText = 'him';
            this.genderAdj = 'he';
          } else {
            this.genderText = 'her';
            this.genderAdj = 'she';
          }
          if (res.isAvailable) {
            this.availMessage = `Dr. ${res.user.firstName.charAt(0).toUpperCase() + res.user.firstName.slice(1)} is available for a call.`;
          } else {
            this.availMessage = `Dr. ${res.user.firstName.charAt(0).toUpperCase() + res.user.firstName.slice(1)}
             is currently unavailable, ${this.genderAdj}
             will get in touch with you as soon as ${this.genderAdj} is available.`;

          }
          this.isEmpty = false;
          this.doctorDetails = res;
          const newStart = Number(res.startTime);
          const workingDays = JSON.parse(res.workingDays);
          const firstWorkDay = workingDays[0];
          const lastWokday = workingDays[workingDays.length - 1];
          this.workDays = `${firstWorkDay} - ${lastWokday}`;
          this.p2pService.intiateConnection(this.doctorDetails.user.email);
          this.getPatientAppointments();
        } else {
          return;
        }
      });
  }

  openProductDetail(id) {
    this.router.navigate(['productDetails', id]);
  }


  gotoPrevious() {
    if (this.businessType === 'Doctor') {
      this.router.navigate(['doctors']);
    } else {
      this.router.navigate(['estore']);

    }
  }

  togglePayment() {
    this.showModal = !this.showModal;
  }
  paymentCancel() {
    console.log('Payment was Canceled');
  }
  paymentDone(event) {
    console.log('Payment Done');
  }
  createReviewDate() {
    this.commentDate = new Date().toDateString();
  }
  getStoreCustomers() {
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    this.storeService.getCustomerById(this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        res.forEach(element => {
          const customer = element.userId;
          if (customer === user.id) {
            this.isRegistered = true;
          }
        });
        this.createCustomer(this.isRegistered);
      });
  }
  createCustomer(isRegistered) {
    this.isRegistered = isRegistered;
    const user = JSON.parse(this.localStorage.getItem('currentUser'));
    if (user !== null) {
      if (this.isRegistered !== true) {
        this.storeService.createCustomer(this.customerForm.value)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(data => {
            alert('Registration Successful');
          },
            error => {
              alert('Something went wrong, Please try again shortly');
            });
      } else {
        alert('You are already a customer of this store');
      }
    } else {
      // alert('You have to log in to become a customer');
      const modalRef = this.modalService.open(LoginComponent);
      modalRef.result.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error);
      });
      // this.router.navigate(['login']);
    }
  }
  async getStoreImage() {
    await this.adminStore.getStoreImageById(this.routeId).subscribe(img => {
      if (img) {
        this.storeImage = 'data:image/png;base64,' + img.file;
      } else {
        return;
      }
    });
  }

  getStoreSearch() {
    this.routeService.filterSearch
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.searchText = data;
      });
  }

  // createAppointmentDetails() {
  //   const userCode = this.userId.substr(0, 4);
  //   const doctorCode = this.doctorDetails.doctorId.substr(0, 4);
  //   const sessionCode = `${userCode}${doctorCode}`;
  //   const appointmentDetail = {
  //     patientId: this.userId,
  //     doctorId: this.doctorDetails.doctorId,
  //     status: 'Pending',
  //     sessionCode: sessionCode,
  //   };
  //   this.showCallDialogue = true;
  // }

  getPatientAppointments() {
    this.storeService.getPatientAppointments(this.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        console.log(res);
        if (res) {
          const appointments = res;
          const docAppointments = appointments.filter(x => x.doctorId === this.doctorDetails.doctorId);
          this.pendingAppointments = docAppointments.filter(y => y.status === 'Pending' || y.status === 'Active');
          if (this.pendingAppointments.length > 0) {
            this.showCallDialogue = true;
          } else {
            this.showCallDialogue = false;
          }
          this.pendingAppointments.forEach(element => {
            this.isPaymentDone = element.isPaymentDone;
            if (!this.isPaymentDone) {
              this.showPayButton = true;
              this.modalMessage = 'Your appointment has been booked. Please complete payment to continue';
            }
          });
        } else {
          return;
        }
      });
  }

  getAppointmentSession() {
    this.storeService.getAppointment(this.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.isPaymentDone = res.isPaymentDone;
        if (res && res.status === 'Pending') {
          this.showCallDialogue = true;
        } else {
          this.showCallDialogue = false;
          return;
        }

      });
  }

  createAppointment(id) {
    const isPaymentDone = false;
    const userCode = this.userId.substr(0, 4);
    const doctorCode = this.doctorDetails.doctorId.substr(0, 4);
    const sessionCode = `${userCode}${doctorCode}`;
    const appointmentDetails = {
      patientId: id,
      doctorId: this.doctorDetails.doctorId,
      status: 'Pending',
      sessionCode: sessionCode,
      patientAilment: this.ailmentForm.value.patientAilment,
      isPaymentDone
    };
    this.storeService.createDoctorAppointment(appointmentDetails)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          localStorage.setItem('sessionId', res.sessionId);
          // alert('Your appointment has been scheduled');
          // this.showCallDialogue = true;
          this.modalMessage = `Your appointment has been booked successfully.
          To proceed to seeing your doctor, please pay a sum of NGN ${this.consultationFee}. Do you want to continue to
          checkout?`;
        } else {
          this.showCallDialogue = false;
        }
      });
  }
  getPatient() {
    this.showModal = true;
    this.storeService.getPatient(this.doctorId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          this.createPatientRecord();
        } else {
          this.createAppointment(res.patientId);
        }
      });
  }


  createPatientRecord() {
    const patientForm = {
      userId: this.userId,
      doctorId: this.doctorDetails.doctorId
    };
    this.storeService.createPatient(patientForm)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          console.log(res);
          this.createAppointment(res.patientId);
        }

      });
  }

  updateAppointment() {
    const payLoad = {
      sessionStartDateTime: '',
      sessionEndDateTime: '',
      status: 'Pending',
      diagnosis: '',
      isPaymentDone: true
    };


    const sessionId = localStorage.getItem('sessionId');
    this.storeService.updateDoctorAppointment(sessionId, payLoad)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.getDoctorDetails();
      });
  }

  callDoctor() {
    this.openModal = true;
    const callDetails = {
      callerId: this.userId,
      callerName: this.userName,
      onCall: true,
      receiverId: this.doctorDetails.doctorId,
      callerImgUrl: this.userImgUrl,
      groupName: this.doctorDetails.user.email,
      receiverFirstName: this.doctorDetails.user.firstName,
      receiverLastName: this.doctorDetails.user.lastName,
      receiverImage: this.doctorDetails.user.imageUrl,
      receiverTitle: 'Dr.',
      patientId: this.userId,
      doctorId: this.doctorDetails.doctorId
    };
    const message = JSON.stringify(callDetails);
    this.p2pService.callGroupMember(this.doctorDetails.user.email, message);
    this.utility.toggleVideo(callDetails);
    this.p2pService.addUserToPeer(true);
    this.utility.playRingBack();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
