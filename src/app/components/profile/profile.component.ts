import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { User } from 'src/app/Model/user';
import { Appointment } from 'src/app/Model/appointment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Doctor } from 'src/app/Model/doctor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  userDetails: User;
  appointments: Appointment[];
  user: User;
  userCode: any;
  recentAppointments: Appointment[];
  recentDoctorsApp = [];
  doctors = [];
  showModal: boolean;
  sessionId: any;
  session: Appointment;
  doctorDetails: Doctor;
  emptyPres: boolean;


  constructor(private authService: AuthenticateDataService,
    private appService: MarketsDataService,
    private router: Router) { }

  ngOnInit() {
    this.getCurrentUser();
  }



  getCurrentUser() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    if (!user) {
      this.router.navigate(['/']);
    } else {
      this.authService.getById(user.userId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
          if (!res) {
            return;
          } else {
            console.log(res);
            this.user = res;
            this.userCode = res.userId.substr(0, 6);
            this.getAllUserAppointments();
          }
        });
    }

  }
  getAllUserAppointments() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.appService.getPatientAppointments(user.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          console.log(res);
          this.appointments = res;
          this.recentAppointments = this.appointments.slice(Math.max(this.appointments.length - 2, 0));
          this.getDoctors();
        } else {
          return;
        }
      }, er => {
        console.error(er.message);
      });
  }

  getDoctors() {
    this.recentAppointments.forEach(element => {
      this.appService.getDoctorDetail(element.doctorId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
          if (!res) {
            return;
          } else {
            this.recentDoctorsApp.push(res);
            this.getAptDoctors();

          }
        });
    });

  }

  getAptDoctors() {
    this.doctors.splice(1);
    this.appointments.forEach(element => {
      this.appService.getDoctorDetail(element.doctorId)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
          if (!res) {
            return;
          } else {
            this.doctors.push(res);
          }
        });
    });
  }

  toggleModal(id) {
    this.showModal = true;

    this.appService.getAppointment(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          this.session = res;

          this.getDoctorDetails(res.doctorId);
        }
      });
  }

  getDoctorDetails(id) {
    this.appService.getDoctorDetail(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          return;
        } else {
          this.doctorDetails = res;
          console.log(this.session, this.doctorDetails);
        }
      });
  }



  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
