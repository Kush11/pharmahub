import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, takeUntil } from 'rxjs/operators';
import { Role } from 'src/app/Model/role';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { Subject } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WINDOW } from '@ng-toolkit/universal';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  data: Object;
  spinner = false;
  private unsubscribe$ = new Subject<void>();

  gender = [
    {
      id: '1',
      gender: 'Male',
    },
    {
      id: '2',
      gender: 'Female',
    },
    {
      id: '3',
      gender: 'Others',
    }
  ];

  genderSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'gender',
    selectAllText: '',
    unSelectAllText: '',
    allowSearchFilter: false,
    closeDropDownOnSelection: true,
  };

  constructor(@Inject(WINDOW) private window: Window, private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public activeModal: NgbActiveModal,
    private authenticate: AuthenticateDataService

  ) {
    // redirect to home if already logged in
    if (this.authenticate.currentUserValue) {
      const role = this.authenticate.decode().role;
      if (role === 'Doctor') {
        this.router.navigate(['admin/doctor', this.authenticate.currentUserValue.userId]);
      } else if (role === 'Merchant') {
        this.router.navigate(['admin/store', this.authenticate.currentUserValue.userId]);
      } else if (role === 'Admin') {
        this.router.navigate(['admin/admin']);
      } else {
        this.router.navigate(['/']);

      }
      console.log(role);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      age: ['', Validators.required],
      password: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', Validators.required],
      role: [Role.User, Validators.required],
      address: ['', Validators.required]

    });




    // get return url from route or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convinience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  // get x() {return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // // stop here if form is invalid
    // if (this.loginForm.invalid) {
    //   return;
    // }
    this.loading = true;
    this.authenticate.login(this.f.email.value, this.f.password.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        data => {
          console.log(data);
          this.loadSpinner();
        },
        error => {
          this.error = error;
          this.loading = false;
        }
      );
  }
  onRegister() {
    this.submitted = true;
    this.loading = true;
    this.authenticate.register(this.loginForm.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        data => {
          alert('Account created sucessfully.');
          this.authenticate.login(this.loginForm.value.email, this.loginForm.value.password)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
              res => {
                this.loading = false;
                this.loadSpinner();
              },
              error => {
                this.error = error;
                this.loading = false;
              }
            );
        },
        error => {
          this.loading = false;
          alert('An error occured. Please try again shortly.');
        }
      );
  }
  closeModal() {
    this.activeModal.close('Modal Closed');
  }
  loadSpinner() {
    this.activeModal.close('Modal Closed');
    this.window.location.reload();
  }
  calculateAge() {
    const currentDate = new Date().getFullYear();
    const dob = new Date(this.loginForm.value.age).getFullYear();
    const age = currentDate - dob;
    this.loginForm.patchValue({
      age: age.toString()
    });
    console.log(this.loginForm.value);
  }
  getGender(value) {
    console.log(value);
    this.loginForm.patchValue({
      gender: value.gender
    });
    this.calculateAge();
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
