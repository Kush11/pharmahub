import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Input, AfterViewInit, AfterViewChecked } from '@angular/core';
import { P2pSevice } from 'src/app/services/Shared/p2p.service';
import { User } from 'src/app/Model/user';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { Role } from 'src/app/Model/role';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { Appointment } from 'src/app/Model/appointment';


@Component({
  selector: 'app-video-chat',
  templateUrl: './video-chat.component.html',
  styleUrls: ['./video-chat.component.css']
})
export class VideoChatComponent implements OnInit, OnDestroy {

  @ViewChild('peerVideo') peerVideo: ElementRef;
  @ViewChild('userVideo') userVideo: ElementRef;
  @Input() groupName: string;
  @Input() callerDetails: any;
  @Input() isCaller: boolean;




  currentUser: any;
  users$: any;
  private stream;
  private unsubscribe$ = new Subject<void>();
  // userVideo: string;
  dataString: string;
  messages = [];
  localStream: MediaStream;
  showVideo: boolean;
  toggleScreen: boolean;
  showCamera: boolean;
  showUserVideo: boolean;
  audioIsON: boolean;
  routeId: any;
  onCall: any;
  openMic: any;
  openCam: any;
  isWaiting: boolean;
  show: boolean;
  endCall: any;
  showEndMessage: boolean;
  endCallMessage: string;
  userRole: any;
  patientMessage: string;
  backGroundImg: any;
  backgroundImg: any;
  connectMessage: string;
  showForm: any;
  diagForm: FormGroup;
  presForm: FormGroup;
  submittedDiagForm: boolean;
  submittedPresForm: boolean;
  patientForm: FormGroup;
  patientDetails: User;
  currentSession: Appointment[];
  status: string;
  currentDate: string;
  sessionId: string;



  constructor(private chatService: P2pSevice,
    private route: Router, private router: ActivatedRoute,
    private auth: AuthenticateDataService,
    private utility: UtilityService,
    private fb: FormBuilder,
    private appointmentService: MarketsDataService) { }

  ngOnInit() {
    this.isWaiting = true;
    this.onCall = true;
    this.utility.toggleAdminHeader(false);
    this.connectMessage = 'Contacting';


    this.users$ = this.chatService.users$;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.getCurrentUserRole();

    this.chatService.signal$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.chatService.signalPeer(res.id, res.data, this.stream);
        } else {
          return;
        }
      });
    this.chatService.onSignalToSend$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.connectMessage = 'Connecting';
        this.dataString = JSON.stringify(res);
        this.chatService.sendSignal(this.groupName, JSON.stringify(res));
      }, err => {
        this.connectMessage = 'Unable to Connect';
      });

    this.chatService.onData$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
      });
    this.chatService.onStream$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.utility.stopAudio();
        this.isWaiting = false;
        this.onCall = true;
        this.peerVideo.nativeElement.muted = false;
        this.peerVideo.nativeElement.srcObject = res.data;
        this.peerVideo.nativeElement.load();
        this.showUserVideo = false;
      });

    this.chatService.onEndSession$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.endCall = res;
          this.endSession();
        } else {
          return;
        }
      });


    this.chatService.patientSignal$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res === 'False') {
          this.utility.stopAudio();
          this.endCall = true;
          this.showMessage('reject');
        }
      });



    if (this.currentUser.userId === this.callerDetails.callerId) {
      this.backgroundImg = this.callerDetails.receiverImage;
    } else {
      this.backgroundImg = this.callerDetails.callerImgUrl;
    }
    this.getPatientDetails();
    this.openVideo();
    this.showCamera = false;

    this.chatService.onCall$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.addPeer();
        } else {
          return;
        }
      });

  }


  toggleFooterAndHeader() {
    this.toggleScreen = !this.toggleScreen;
    this.utility.toggleHeader(this.toggleScreen);
  }
  toggleForm() {
    this.showForm = !this.showForm;
  }

  addPeer() {
    const peer = this.chatService.createPeer(this.stream, this.groupName, true);
    this.chatService.currentPeer = peer;
  }


  createPrescription() {
    this.presForm.patchValue({
      sessionId: this.sessionId,
    });
    this.appointmentService.createPrescription(this.presForm.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.submittedPresForm = true;
          alert('Your patient report form has been submitted sucessfully.');
          this.showForm = false;
        } else {
          this.submittedPresForm = false;
          return;
        }
      }, er => {
        alert(er.message);
      });
  }


  openVideo() {
    try {
      navigator.mediaDevices.getUserMedia({ video: true, audio: true })
        .then(stream => {
          this.showUserVideo = true;
          this.localStream = stream;
          this.stream = stream;
          this.userVideo.nativeElement.muted = true;
          this.userVideo.nativeElement.srcObject = stream;
          this.userVideo.nativeElement.load();
          const playPromise = this.userVideo.nativeElement.play();
          playPromise.then(_ => {
            console.log(_);
          })
            .catch(error => {
              console.log(error);
            });
          this.showCamera = false;
          this.utility.toggleHeader(false);
        });

      // this.videoPlayer.nativeElement.srcObject = this.stream;

      // this.chatService.signalPeer(res.id, JSON.stringify(res), this.stream);

    } catch (error) {
      console.error(`cant join room , error ${error}`);
    }
  }

  async sendMessage(): Promise<void> {
    this.chatService.sendMessage(this.dataString);
    this.messages = [...this.messages, { own: true, message: this.dataString }];
    this.dataString = null;

  }

  getCurrentUserRole() {
    const decode = this.auth.decode();
    console.log(decode);
    this.userRole = decode.role;
  }

  toggleCamera() {
    this.openCam = !this.openCam;
    this.localStream.getVideoTracks()[0].enabled = !this.localStream.getVideoTracks()[0].enabled;

  }

  toggleAudio() {
    this.openMic = !this.openMic;
    this.localStream.getAudioTracks()[0].enabled = !this.localStream.getAudioTracks()[0].enabled;
  }

  toggleCall() {
    this.endSessionForm();
    this.endSession();
    this.sendEndCallMessage();
  }

  showMessage(type) {
    if (type === 'end') {
      if (this.currentUser.userId === this.callerDetails.callerId) {
        this.endCallMessage = `Your call with ${this.callerDetails.receiverTitle}
       ${this.callerDetails.receiverFirstName} ${this.callerDetails.receiverLastName} has ended.
    `;
      } else {
        this.endCallMessage = `Your call with ${this.callerDetails.callerName} has ended.`;
      }

      if (this.userRole !== Role.Doctor) {
        this.patientMessage = 'Please check your profile page for information on billing and prescription.';
      }
    } else {
      if (this.currentUser.userId === this.callerDetails.callerId) {
        this.endCallMessage = `${this.callerDetails.receiverTitle}
       ${this.callerDetails.receiverFirstName} ${this.callerDetails.receiverLastName} declined your call.
        Please call again or try other doctors.
    `;
      } else {
        this.endCallMessage = `${this.callerDetails.callerName} declined your call.
        Please call again.`;
      }
    }

    this.showEndMessage = true;

  }

  endSession() {
    this.localStream.getAudioTracks()[0].stop();
    this.localStream.getVideoTracks()[0].stop();
    this.showMessage('end');
  }

  closeVideoScreen() {
    if (this.userRole === Role.Doctor) {
      this.route.navigate([`admin/doctor/${this.callerDetails.doctorId}`]);
    } else {
      this.route.navigate(['/profile']);
    }
    this.utility.toggleVideo(false);
  }

  sendEndCallMessage() {
    this.chatService.endGroupCall(this.callerDetails.groupName, true);
  }

  getPatientDetails() {
    this.auth.getById(this.callerDetails.patientId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.patientDetails = res;
          this.getAppointmentDetails();
        } else {
          return;
        }
      });
  }

  getAppointmentDetails() {
    this.appointmentService.getDoctorAppointments(this.callerDetails.doctorId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          console.log(res);
          this.currentSession = res.filter(x => x.patient.userId === this.callerDetails.patientId &&
            x.status === 'Pending' || x.status === 'Active');
          if (this.currentSession.length > 0) {
            const sessionId = this.currentSession[0].sessionId;
            this.getCurrentDateTime();
            this.formData(sessionId);
          }
        } else {
          return;
        }
      });
  }

  getCurrentDateTime() {
    this.currentDate = new Date().toUTCString();
  }


  formData(id) {
    const status = 'Active';
    this.patientForm = this.fb.group({
      patientFirstName: [this.patientDetails.firstName],
      patientLastName: [this.patientDetails.lastName],
      patientAge: [this.patientDetails.age],
      patientGender: [this.patientDetails.gender]
    });

    this.diagForm = this.fb.group({
      sessionStartDateTime: [this.currentDate, Validators.required],
      sessionEndDateTime: [''],
      status: [status, Validators.required],
      diagnosis: [''],
    });

    this.presForm = this.fb.group({
      sessionId: ['', Validators.required],
      comment: ['', Validators.required]
    });
    this.updateDiagForm(id, this.diagForm.value);
  }

  updateDiagForm(id, payload) {
    if (this.userRole === Role.Doctor) {
      this.appointmentService.updateDoctorAppointment(id, payload)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res => {
        });
    }
  }



  // addPresForm() {
  //   this.medicineNames.push(this.fb.group({ medecineName: '' }));
  //   this.dosages.push(this.fb.group({ dosage: '' }));
  // }

  // removePresForm(index) {
  //   this.medicineNames.removeAt(index);
  //   this.dosages.removeAt(index);
  // }

  submitDiagnosisForm() {
    this.appointmentService.getDoctorAppointments(this.callerDetails.doctorId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          const currentSession = res.filter(x => x.patientId === this.callerDetails.patientId &&
            x.status === 'Pending' || x.status === 'Active');
          const currentDate = currentSession[0].sessionStartDateTime;
          this.sessionId = currentSession[0].sessionId;
          this.diagForm.patchValue({
            sessionStartDateTime: currentDate,
            sessionEndDateTime: '',
            status: 'Active',
            diagnosis: this.diagForm.value.diagnosis
          });
          this.createPrescription();
          this.updateDiagForm(this.sessionId, this.diagForm.value);
        }
      });

  }

  endSessionForm() {
    this.getCurrentDateTime();
    this.appointmentService.getDoctorAppointments(this.callerDetails.doctorId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          const currentSession = res.filter(x => x.patientId === this.callerDetails.patientId &&
            x.status === 'Pending' || x.status === 'Active');
          const currentDate = currentSession[0].sessionStartDateTime;
          const sessionId = currentSession[0].sessionId;
          this.diagForm.patchValue({
            sessionStartDateTime: currentDate,
            sessionEndDateTime: this.currentDate,
            status: 'Completed',
            diagnosis: this.diagForm.value.diagnosis
          });
          this.updateDiagForm(sessionId, this.diagForm.value);
        }
      });
  }





  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
