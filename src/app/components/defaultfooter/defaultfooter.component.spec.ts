import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultfooterComponent } from './defaultfooter.component';

describe('DefaultfooterComponent', () => {
  let component: DefaultfooterComponent;
  let fixture: ComponentFixture<DefaultfooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultfooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultfooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
