import { Component, OnInit, OnDestroy } from '@angular/core';
import { UtilityService } from 'src/app/services/Shared/utility.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-defaultfooter',
  templateUrl: './defaultfooter.component.html',
  styleUrls: ['./defaultfooter.component.css']
})
export class DefaultfooterComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  showFooter: boolean;



  constructor(private utility: UtilityService) { }

  ngOnInit() {
    this.utility.showFooter.pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.showFooter = res;
      });

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


}
