import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminDataService } from 'src/app/admin/admin-services/admin.data.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterStoreComponent } from '../register-store/register-store.component';

@Component({
  selector: 'app-estore',
  templateUrl: './estore.component.html',
  styleUrls: ['./estore.component.css']
})
export class EstoreComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  allEStores: any;
  emptyMarket: boolean;
  searchText: any;
  loadSpinner: boolean;

  constructor(private adminService: AdminDataService,
    private router: Router,
    private modalService: NgbModal) { }

  ngOnInit() {

    this.getAllEstores();

  }

  getAllEstores() {
    this.loadSpinner = true;
    this.adminService.getStores()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (!res) {
          this.emptyMarket = true;
          this.loadSpinner = false;
        } else {
          const allStores = res;
          // const estores = allStores.filter(s => s.storeType !== 'physical store')
          this.allEStores = allStores.filter(cs => cs.confirmStatus === true);
          this.loadSpinner = false;
        }
      });
  }

  getStoresId(id) {
    this.router.navigate(['business', id], { queryParams: { businessType: 'Merchant' } });
  }
  openFormModal() {
    const modalRef = this.modalService.open(RegisterStoreComponent);

    modalRef.result.then((result) => {
    }).catch((error) => {
      console.log(error);
    });
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
