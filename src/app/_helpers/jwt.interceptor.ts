import { AuthenticateWebService } from './../services/Authentication/authentication.web.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticateWebService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = this.authenticationService.currentUserValue;
    const secKey = localStorage.getItem('secKey').toString();
    if (request.url === environment.raveStandardEndpoint) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${secKey}`,
        }
      });
    } else {
      if (currentUser) {
        if (currentUser.token) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${currentUser.token}`
            }
          });
        }
      }
    }
    return next.handle(request);
  }
}

