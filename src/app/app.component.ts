import { AuthenticateWebService } from './services/Authentication/authentication.web.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute, NavigationEnd } from '@angular/router';
import { User } from './Model/user';
import { Role } from './Model/role';
import { Observable, Subject } from 'rxjs';
import { WINDOW } from '@ng-toolkit/universal';
import { P2pSevice } from './services/Shared/p2p.service';
import { UtilityService } from './services/Shared/utility.service';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  showHead: boolean;
  currentUser: User;
  isLoggedIn$: Observable<boolean>;
  routeId: number;
  role: any;
  decode: any;
  private unsubscribe$ = new Subject<void>();
  showVideo: any;
  currentUserRole: any;
  groupName: any;
  addPeer: boolean;
  callerDetails: any;
  isCaller: boolean;


  constructor(@Inject(WINDOW) private window: Window, private router: Router,
    private authenticate: AuthenticateWebService,
    private route: ActivatedRoute,
    private chatService: P2pSevice,
    private utility: UtilityService) {

    // on route change to '/login', set the variable showHead to false
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event.url === '/login' || event.url.startsWith('/admin')) {
          this.showHead = false;
          this.utility.toggleHeader(this.showHead);
          this.utility.toggleFooter(this.showHead);
        } else {
          this.showHead = true;
          this.utility.toggleHeader(this.showHead);
          this.utility.toggleFooter(this.showHead);
        }
      }
    });
  }


  ngOnInit() {
    this.getCurrentUser();
    this.showVideo = false;

    this.utility.showHeader
      .subscribe(res => {
        this.showHead = res;
      });


    this.utility.openVideo
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        if (res) {
          this.callerDetails = res;
          if (this.currentUser.userId === res.callerId) {
            this.isCaller = true;
          } else {
            this.isCaller = false;
          }
          this.showVideo = true;
          this.groupName = res.groupName;
          this.showHead = false;
          this.chatService.startCall(res.groupName, true);
          setTimeout(() => {
            this.chatService.addUserToPeer(true);
          }, 5000);
        } else {
          this.showVideo = false;
          if (this.router.url.startsWith('/admin')) {
            this.showHead = false;
          } else {
            this.showHead = true;
          }
        }
      });


    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.window.scrollTo(0, 0);
    });

    this.route.params.subscribe(p => {
      this.routeId = +p['id'];
    });


  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.currentUserRole = this.authenticate.decode();
      if (this.currentUserRole.role === 'Doctor') {
        this.chatService.intiateConnection(this.currentUser.email);
      }
    } else {
      return;
    }
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }

  logout() {
    this.authenticate.logout();
    this.router.navigate(['/login']);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
