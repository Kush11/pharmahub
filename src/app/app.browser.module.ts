import { AdminWebService } from './admin/admin-services/admin.web.service';
import { AddProductComponent } from './admin/admin-merchant/add-product/add-product.component';
import { StarRatingModule } from 'angular-star-rating';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AuthenticateWebService } from 'src/app/services/Authentication/authentication.web.service';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { CartService } from './services/Shared/cart.service';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DefaultheaderComponent } from './components/defaultheader/defaultheader.component';
import { DefaultfooterComponent } from './components/defaultfooter/defaultfooter.component';
import { HomescreenComponent } from './components/homescreen/homescreen.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ContactComponent } from './components/contact/contact.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { DirectionsComponent } from './components/directions/directions.component';
import { HowtoComponent } from './components/howto/howto.component';
import { PolicyComponent } from './components/policy/policy.component';
import { AboutComponent } from './components/about/about.component';
import { TermsComponent } from './components/terms/terms.component';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreComponent } from './components/store/store.component';
import { MarketsWebService } from './services/Markets/markets.web.service';
import { AuthenticateDataService } from './services/Authentication/authentication.data.service';
import { StoreService } from './services/Shared/store.service';
import { Angular4PaystackModule } from 'angular4-paystack';
import { RegisterStoreComponent } from './components/register-store/register-store.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RatingComponent } from './rating/rating.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AdminDataService } from './admin/admin-services/admin.data.service';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { Resolver } from './resolver/resolver';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { EstoreComponent } from './components/estore/estore.component';
import { OrderModule } from 'ngx-order-pipe';
import { AdsenseModule } from 'ng2-adsense';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { AppModule } from './app.module';
@NgModule({
  imports: [
    BrowserModule,
    DeferLoadModule,
    StarRatingModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Angular4PaystackModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    AdsenseModule.forRoot({
      adClient: 'ca-pub-8198395832831199',
      adSlot: 3761928729,
    }),
    OrderModule,
    CommonModule,
    TransferHttpCacheModule,
    NgtUniversalModule,
    AppModule,
    BrowserTransferStateModule

  ],
  providers: [
    CartService,
    StoreService,
    NgbActiveModal,
    Resolver,


    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: MarketsDataService, useClass: MarketsWebService },
    { provide: AuthenticateDataService, useClass: AuthenticateWebService },
    { provide: AdminDataService, useClass: AdminWebService }

  ],
  bootstrap: [AppComponent],
  entryComponents: [RegisterStoreComponent, AddProductComponent, LoginComponent]

})
export class AppBrowserModule { }
