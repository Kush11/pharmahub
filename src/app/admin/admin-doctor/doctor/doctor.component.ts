import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Doctor } from 'src/app/Model/doctor';
import { Subject } from 'rxjs';
import { Appointment } from 'src/app/Model/appointment';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { P2pSevice } from 'src/app/services/Shared/p2p.service';
import { UtilityService } from 'src/app/services/Shared/utility.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit, OnDestroy {



  userId: string;
  doctorDetail: Doctor;
  private unsubscribe$ = new Subject<void>();
  currentAppointment: Appointment;
  appointments: Appointment[];
  showHeader: any;


  constructor(private router: ActivatedRoute,
    private doctorService: MarketsDataService,
    private p2pService: P2pSevice,
    private utility: UtilityService) { }

  ngOnInit() {
    this.utility.toggleAdminHeader(true);
    this.router.params.subscribe(p => {
      this.userId = p.id;
      this.getDoctorDetail();
    });
    this.utility.showAdminHeader
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.showHeader = res;
      });
  }

  getDoctorDetail() {
    this.doctorService.getDoctorDetail(this.userId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.doctorDetail = res;
        // this.p2pService.intiateConnection(this.doctorDetail.user.email);
        this.doctorService.getDoctorAppointments(res.doctorId)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(app => {
            this.appointments = app;
          });
      });

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }



}
