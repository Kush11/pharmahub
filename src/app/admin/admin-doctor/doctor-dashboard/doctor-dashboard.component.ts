import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MerchantService } from '../../admin-merchant/merchant.service';
import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { Doctor } from 'src/app/Model/doctor';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Appointment } from 'src/app/Model/appointment';
import { AuthenticateDataService } from 'src/app/services/Authentication/authentication.data.service';
import { P2pSevice } from 'src/app/services/Shared/p2p.service';
import { User } from 'src/app/Model/user';
import { UtilityService } from 'src/app/services/Shared/utility.service';

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.css']
})
export class DoctorDashboardComponent implements OnInit, OnDestroy {
  @Input() doctorDetail: Doctor;
  @Input() appointments: Appointment[];

  userId: string;
  private unsubscribe$ = new Subject<void>();
  currentAppointment: Appointment;
  appointmentsCount: number;
  pendingAppointments: Appointment[];
  completedAppointments: Appointment[];
  pendingAppointmentsCount: number;
  completedAppointmentsCount: number;
  onGoingCall: boolean;
  openModal: boolean;
  patient: User;
  groupName: string;
  openVideo: boolean;
  page = 4;
  patients = [];
  patientCount: number;
  activeAppointments: Appointment[];
  activeAppointmentsCount: number;
  patientCode: string;
  p = 1;




  constructor(private router: ActivatedRoute,
    private authService: AuthenticateDataService,
    private route: Router,
    private storeService: MarketsDataService,
    private p2pService: P2pSevice,
    private utility: UtilityService) { }

  ngOnInit() {
    this.openVideo = false;
    this.router.params.subscribe(p => {
      this.userId = p.id;
      this.getDoctorDetail();
    });
  }

  getDoctorDetail() {
    this.pendingAppointments = this.appointments.filter(x => x.status === 'Pending');
    this.completedAppointments = this.appointments.filter(x => x.status === 'Completed');
    this.activeAppointments = this.appointments.filter(x => x.status === 'Active');
    this.pendingAppointmentsCount = this.pendingAppointments.length;
    this.completedAppointmentsCount = this.completedAppointments.length;
    this.appointmentsCount = this.appointments.length;
    this.activeAppointmentsCount = this.activeAppointments.length;
    this.getAllPatients();
  }

  // getDoctorPatients() {
  //   const patients = this.appointments.map(item => item.patient.email)
  //     .filter((value, index, self) => self.indexOf(value) === index);
  //   this.patientCount = patients.length;
  // }

  getAllPatients() {
    this.storeService.getAllPatientsByDoctorId(this.doctorDetail.doctorId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.patientCount = res.length;
        this.patients = res;
        res.forEach(element => {
          this.patientCode = element.patientId.substr(0, 6);
        });
      });
  }



  acceptCall() {
    // this.p2pService.showStream();
    // this.openModal = false;
  }

  findUnique(arr, predicate) {
    const found = {};
    arr.forEach(d => {
      found[predicate(d)] = d;
    });
    return Object.keys(found).map(key => found[key]);
  }





  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onLogout() {
    this.route.navigate(['/']);
  }

}
