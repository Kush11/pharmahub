
import { MerchantService } from './admin-merchant/merchant.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import { MerchantComponent } from './admin-merchant/merchant/merchant.component';
import { Angular4PaystackModule } from 'angular4-paystack';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../_helpers/jwt.interceptor';
import { ErrorInterceptor } from '../_helpers/error.interceptor';
import { AdminDataService } from './admin-services/admin.data.service';
import { AdminWebService } from './admin-services/admin.web.service';
import { DashboardComponent } from './admin-merchant/dashboard/dashboard.component';
import { AdminComponent } from './site-admin/admin/admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';
import { DoctorComponent } from './admin-doctor/doctor/doctor.component';
import { DoctorDashboardComponent } from './admin-doctor/doctor-dashboard/doctor-dashboard.component';
import { VideoChatComponent } from '../components/video-chat/video-chat.component';
import { AppModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';




@NgModule({
  declarations: [MerchantComponent, DashboardComponent, AdminComponent, DoctorComponent,
    DoctorDashboardComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    Angular4PaystackModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPaginationModule,


  ],
  providers: [
    MerchantService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: AdminDataService, useClass: AdminWebService },


  ],

})
export class AdminModule { }
