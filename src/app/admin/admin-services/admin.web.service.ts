
import { ProductAds, Products } from './../../Model/products';
import { Markets } from './../../Model/markets';
import { Stores, StoreAds } from 'src/app/Model/stores';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable, } from '@angular/core';
import { AdminDataService } from './admin.data.service';
import { User } from 'src/app/Model/user';
import { map, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';




@Injectable({ providedIn: 'root' })
export class AdminWebService implements AdminDataService {



  config: any;
  progress: number;
  message: string;
  webUrl: string;
  mockUrl: string;

  constructor(private http: HttpClient) {
    this.config = new HttpHeaders().set('Content-Type', 'application/json')
      .set('Accept', 'application/json');
    this.webUrl = environment.webUrl;

    // this.mockUrl = environment.mockWebUrl;
  }




  getUsers() {
    return this.http.get<User[]>(`${this.webUrl}/user`);
  }
  getUserById(id: any) {
    return this.http.get<User>(`${this.webUrl}/user/${id}`);
  }
  updateUser(user: User) {
    return this.http.put<User>(`${this.webUrl}/user/${user.userId}`, user);
  }
  deleteUser(id: any) {
    return this.http.delete(`${this.webUrl}/user/${id}`);
  }
  createUser(user: User) {
    return this.http.post<User>(`${this.webUrl}/user`, user);
  }


  getStores() {
    return this.http.get(`${this.webUrl}/stores`)
  }
  createStores(store: Stores) {
    return this.http.post<Stores>(`${this.webUrl}/stores`, store);
  }
  updateStore(store: any, id: any) {
    return this.http.put(`${this.webUrl}/stores/${id}`, store);
  }
  deleteStore(id: any) {
    return this.http.delete(`${this.webUrl}/stores/${id}`);
  }
  getStoresById(id: any) {
    return this.http.get<any>(`${this.webUrl}/stores/${id}`);
  }


  getMarkets() {
    return this.http.get(`${this.webUrl}/markets`);
  }
  getMarketById(id: any) {
    return this.http.get(`${this.webUrl}/markets/${id}`);
  }
  createMarket(market: Markets) {
    return this.http.post<Markets>(`${this.webUrl}/markets`, market);
  }
  updateMarket(market: Markets) {
    return this.http.put<Markets>(`${this.webUrl}/markets/${market.id}`, market);
  }
  deleteMarket(id: any) {
    return this.http.delete(`${this.webUrl}/markets/${id}`);
  }

  getOrderDetails() {
    return this.http.get(`${this.webUrl}/orders`);
  }

  getPaymentDetails() {
    return this.http.get(`${this.webUrl}/user`);
  }



  getProducts() {
    return this.http.get(`${this.webUrl}/products`);
  }
  getProductById(id: any) {
    return this.http.get<Products>(`${this.webUrl}/products/${id}`);
  }
  addProduct(product: Products) {
    console.log('from service', product);
    return this.http.post<Products>(`${this.webUrl}/products`, product);
  }
  updateProduct(product: Products) {
    return this.http.put<Products>(`${this.webUrl}/products/${product.Id}`, JSON.stringify(product));
  }
  deleteProduct(id: any) {
    return this.http.delete(`${this.webUrl}/products/${id}`);
  }
  uploadStoreImage(file: any) {
    return this.http.post<string>(`${this.webUrl}/stores/images`, file)
      .pipe(
        map(info => {
          return info;
        })
      );
  }
  updateStoreImage(file: any, Id: any) {
    console.log('file', file)
    return this.http.put(`${this.webUrl}/stores/images/${Id}`, file);
  }
  getStoreImage() {
    return this.http.get(`${this.webUrl}/stores/images`);
  }
  getStoreImageById(id) {
    return this.http.get(`${this.webUrl}/stores/images/${id}`);
  }
  uploadFile(file: any) {
    return this.http.post<string>(`${this.webUrl}/products/productImage`, file)
      .pipe(
        map(info => {
          return info;
        })
      );
  }
  getImage() {
    return this.http.get(`${this.webUrl}/products/productImage`);
  }
  uploadMarketImage(file: any) {
    return this.http.post<string>(`${this.webUrl}/marketbanner`, file)
      .pipe(
        map(info => {
          return info;
        })
      );
  }

  uploadHero(file: any) {
    return this.http.post<string>
      (`${this.webUrl}/hero`, file)
      .pipe(
        map(info => {
          return info;
        })
      );
  }
  getHero() {
    return this.http.get(`${this.webUrl}/hero`);
  }

  updateHero(file: any, id: any) {
    return this.http.put(`${this.webUrl}/hero/${id}`, file)
  }
  getOrderByStoreId(id) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get<Stores>(`${this.webUrl}/orders/store`, { headers: this.config, params: params });
  }
  getOrders() {
    return this.http.get(`${this.webUrl}/orders`);
  }

  getImageById(id) {
    return this.http.get(`${this.webUrl}/products/productImage/${id}`);
  }
  fundWallet(wallet: any) {
    return this.http.post<string>(`${this.webUrl}/store/wallet`, wallet);
  }
  getWallet(id) {
    const params = new HttpParams().set('id', `${id}`);
    return this.http.get(`${this.webUrl}/store/wallet`, { headers: this.config, params: params });
  }
  updateWallet(wallet: any, id: any) {
    return this.http.put(`${this.webUrl}/store/wallet/${id}`, wallet);
  }
  postProductAds(product: any) {
    return this.http.post<ProductAds>(`${this.webUrl}/product/promote`, product, { headers: this.config });
  }
  getProductAds() {
    return this.http.get(`${this.webUrl}/product/promote`);
  }
  getProductAdsById(id: any) {
    return this.http.get(`${this.webUrl}/product/promote/${id}`);
  }

  postStoreAds(store: StoreAds) {
    return this.http.post<StoreAds>(`${this.webUrl}/store/promote`, JSON.stringify(store), { headers: this.config });
  }
  getStoreAds() {
    return this.http.get(`${this.webUrl}/store/promote`);
  }
  getStoreAdsById(id: any) {
    return this.http.get(`${this.webUrl}/store/promote/${id}`);
  }

  postPromotionPackage(promotion) {
    return this.http.post(`${this.webUrl}/admin/promote`, JSON.stringify(promotion), { headers: this.config });
  }
  getPromotionPackages() {
    return this.http.get(`${this.webUrl}/admin/promote`);
  }
  getPromotionPackagesById(id: any) {
    return this.http.get(`${this.webUrl}/admin/promote/${id}`);
  }
  updatePromotionPackages(promotion: any, id: any) {
    return this.http.put(`${this.webUrl}/admin/promote/${id}`, promotion);
  }
  deletePromotionPackages(id) {
    return this.http.delete(`${this.webUrl}/admin/promote/${id}`);
  }
  deleteStoreAds(id) {
    return this.http.delete(`${this.webUrl}/store/promote/${id}`);
  }
  deleteProductAds(id) {
    return this.http.delete(`${this.webUrl}/product/promote/${id}`);
  }
  deleteDiscountAds(id) {
    return this.http.delete(`${this.webUrl}/product/promote/${id}`);
  }
}
