import { AdminComponent } from './site-admin/admin/admin.component';
import { AuthGuard } from './../_guard/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchantComponent } from './admin-merchant/merchant/merchant.component';
import { DoctorComponent } from './admin-doctor/doctor/doctor.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { role: 'Admin' }
  },
  {
    path: 'store/:id',
    component: MerchantComponent,
    canActivate: [AuthGuard],
    data: { role: 'Merchant' },
  },
  {
    path: 'doctor/:id',
    component: DoctorComponent,
    canActivate: [AuthGuard],
    data: { role: 'Doctor' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
