import { MarketsDataService } from 'src/app/services/Markets/markets.data.service';
import { StoreService } from 'src/app/services/Shared/store.service';
import { AddProductComponent } from './../add-product/add-product.component';
import { Keys } from './../../../Model/keys';
import { takeUntil, take } from 'rxjs/operators';
import { AuthenticateDataService } from '../../../services/Authentication/authentication.data.service';

import { ActivatedRoute, Router } from '@angular/router';
import { MerchantService } from './../merchant.service';
import { Component, OnInit, Input, OnDestroy, Inject } from '@angular/core';
import { Subscription, Subject, Observable } from 'rxjs';
import { AdminDataService } from '../../admin-services/admin.data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterStoreComponent } from 'src/app/components/register-store/register-store.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  confirmLoad: boolean;
  storesSubscription: Subscription;
  stores: any;
  customerCount: any = 0;
  products: any;
  productCount: any = 0;
  email: any;
  referenceString: any;
  reference: string;
  routeId: any;
  private unsubscribe$ = new Subject<void>();
  currentUserId: any;
  testKey: string;
  paymentMethod: string;
  liveKey: string;
  amount: number;
  amountToPay: any;
  timer: any;
  paymentForm: FormGroup;
  promoteProductForm: FormGroup;
  promoteDiscountForm: FormGroup;
  updateStoreForm: FormGroup;
  promoteStoreForm: FormGroup;
  orderCount: any = 0;
  orders: any;
  confirmedOrderProducts: any = [];
  confirmedOrderCustomer: any = [];
  productUrl: {};
  active = true;
  storeImage: any;
  orderStatus = {};
  actualPrice: number;
  storeForm: { storeId: any };
  onClickWallet: boolean;
  paymentStatus: any;
  balance: number;
  updatedBalance: number;
  showStoreAds: boolean;
  showProductAds: boolean;
  showDiscountAds: boolean;
  customers: any;
  promotionPackage: any;
  selectedPackage: any;
  showSelectedPackage: boolean;
  adsStartDate: string;
  endAdsDate: string;
  isSelectedProduct: boolean;
  promoProduct: any;
  showSelectedPromoPackage: boolean;
  showSelectedProductPromoPackage: boolean;
  productDiscount: any;
  showSelectedDiscountPromoPackage: boolean;
  isSelectedDiscount: boolean;
  promoDiscount: any;
  status = [];
  showStatus = [];
  loading: boolean;
  pendingOrders: any;
  confirmedOrders: any;
  adsIsActive: boolean;
  storeAdsIsActive: boolean;
  storeAds: any;
  pendingOrderProducts = [];
  pendingOrderCustomer = [];
  walletBalance: any;
  walletBal: any;
  noProductDiscount: any;

  constructor(@Inject(LOCAL_STORAGE) private localStorage: any,
    private storeService: StoreService,
    private route: ActivatedRoute,
    private adminService: AdminDataService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private marketService: MarketsDataService
  ) { }

  ngOnInit() {
    this.localStorage.removeItem('walletBalance');
    this.onClickWallet = false;
    this.localStorage.removeItem('amount');
    this.route.params.subscribe(p => {
      this.routeId = p['id'];
      this.localStorage.setItem('storeId', this.routeId);
    });
    this.getStoreImage();
    this.getPromoPackages();
    this.getProductAds();

    // this.getProducts();
    this.openPaystack();
    this.paymentForm = this.formBuilder.group({
      amount: ['', Validators.required]
    });
    this.promoteProductForm = this.formBuilder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      productId: ['', Validators.required],
      promotionId: ['', Validators.required],
      status: ['Pending'],
      discount: [false],
      // storeId: [this.routeId]
    });
    this.storeForm = {
      storeId: this.routeId
    };
    this.updateStoreForm = this.formBuilder.group({
      storeName: ['', Validators.required],
      address: ['', Validators.required],
      description: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      confirmStatus: [true]
    });
    this.promoteStoreForm = this.formBuilder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      status: ['Pending'],
      promotionId: ['', Validators.required],
      storeId: [this.routeId]
    });
    this.promoteDiscountForm = this.formBuilder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      promotionId: ['', Validators.required],
      productId: ['', Validators.required],
      status: ['Pending'],
      discount: [true],
      storeId: [this.routeId]
    });
    this.getDashboardData();
    this.getBalance();
    this.getStoreAdsStatus();
  }

  onClickFundWallet() {
    this.onClickWallet = !this.onClickWallet;
  }
  async getStoreAdsStatus() {
    await this.adminService
      .getStoreAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        const storeAds = data;
        storeAds.forEach(element => {
          this.storeAds = storeAds.filter(s => s.storeId === this.routeId);
          if (this.storeAds.length > 0) {
            this.storeAdsIsActive = true;
          }
        });
      });
  }
  async getProductAds() {
    await this.adminService
      .getProductAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        res.forEach(element => {
          const productAds = res.filter(
            s => s.products.storeId === this.routeId
          );
          const featuredProducts = productAds.filter(p => p.discount === false);
          const featuredDiscount = productAds.filter(d => d.discount === true);

          if (featuredProducts.length > 0 || featuredDiscount.length > 0) {
            this.adsIsActive = true;
          }
        });
      });
  }
  async getDashboardData() {
    this.confirmLoad = true;
    this.localStorage.removeItem('promoPrice');
    this.adminService
      .getStoresById(this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.stores = data;
        const userId = this.stores.user.userId;
        const currentUser = JSON.parse(this.localStorage.getItem('currentUser'));
        this.currentUserId = currentUser.id;
        this.marketService.getCustomerById(this.routeId)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(cust => {
            if (cust !== null) {
              this.customers = cust;

              this.customerCount = this.customers.length;
              this.productCount = this.stores.products.length;
              this.email = this.stores.email;
              this.products = this.stores.products;
              this.noProductDiscount = this.products.filter(dis => dis.discount > 0);
              this.productDiscount = this.products.filter(dis => dis.discount > 0);

            } else {
              return;
            }
          }, error => {
            alert('We could not fetch your customers record, please try again later');
          });
      });
    await this.adminService
      .getOrders()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.orders = data.filter(c => c.storeId === this.routeId);
        this.pendingOrders = this.orders.filter(
          p => p.orderStatus === 'Pending'
        );
        this.confirmedOrders = this.orders.filter(
          o => o.orderStatus === 'Sold'
        );
        this.orderCount = this.pendingOrders.length;


        // loop through pending orders

        this.pendingOrders.forEach(element => {
          const pendingOrderProduct = element.productId;
          const pendingOrderCustomer = element.customerId;
          this.adminService
            .getProductById(pendingOrderProduct)
            .subscribe(product => {
              const pendingOrderProducts = product;
              this.pendingOrderProducts.push(pendingOrderProducts);
            });
          this.adminService
            .getUserById(pendingOrderCustomer)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(cust => {
              if (cust) {
                const orderCustomers = cust;
                this.pendingOrderCustomer.push(orderCustomers);
              } else {
                alert('The customer does not exist or has been deleted from this store');
                return;
              }

            });
        });

        // loop through confirmed orders
        this.confirmedOrderCustomer.splice(0);
        this.confirmedOrderProducts.splice(0);
        this.confirmedOrders.forEach(element => {
          const confirmedOrderProduct = element.productId;
          const confirmedOrderCustomer = element.customerId;
          this.adminService
            .getProductById(confirmedOrderProduct)
            .subscribe(product => {
              const confirmedOrderProducts = product;
              this.confirmedOrderProducts.push(confirmedOrderProducts);
            });
          this.adminService
            .getUserById(confirmedOrderCustomer)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(cust => {
              const confirmedOrderCustomers = cust;
              this.confirmedOrderCustomer.push(confirmedOrderCustomers);
            });
        });
        setTimeout(() => {
          this.confirmLoad = false;
        }, 10000);
      });
    this.getBalance();
  }

  onKey(event) {
    console.log(event.code);
    const inputValue = event.target.value;
    const actualPrice = inputValue * 100;
    this.actualPrice = actualPrice;
  }
  openPaystack() {
    this.amount = this.actualPrice;
    this.reference = '' + Math.floor(Math.random() * 1000000000 + 1);
    this.testKey = new Keys().TestKey;
    this.liveKey = new Keys().LiveKey;
    this.paymentMethod = new Keys().PaymentType;
    this.localStorage.removeItem('amount');
  }
  paymentCancel() {
    this.onClickWallet = false;
    this.localStorage.removeItem('amount');
  }

  paymentDone(event) {
    clearTimeout(this.timer);
    this.onClickWallet = false;
    this.paymentStatus = event.status;
    this.storeWalletValue();
  }
  openProductModal() {
    const modalRef = this.modalService.open(AddProductComponent);
    modalRef.result
      .then(result => { })
      .catch(error => {
        console.log(error);
      });
  }
  editProduct() { }
  async updateStoreProfile() {
    await this.adminService
      .updateStore(this.updateStoreForm.value, this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        res => {
          this.adminService
            .getStores()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(data => {
              alert('Store details updated successfully');
            });
        },
        error => {
          alert('An error occured, please try again shortly');
        }
      );
  }
  delete(id, i) {
    this.adminService
      .deleteProduct(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        response => {
          this.products.splice(i, 1);
        },
        error => console.log('There was an error: ', error)
      );
  }

  uploadStorePhoto(event) {
    this.getStoreImage();
    if (this.storeImage == null) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async () => {
        const file = reader.result.toString().split(',')[1];
        const model = { File: file };
        this.productUrl = model.File;
        const image = { file: this.productUrl, storeId: this.routeId };
        this.adminService.uploadStoreImage(image).subscribe(
          data => {
            this.loading = true;
            alert('Store profile picture uploaded successfully');
          },
          error => {
            alert('An error occured, please try again shortly');
          }
        );
      };
      setTimeout(() => {
        this.getStoreImage();
        this.loading = false;
      }, 15000);
    } else {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = async () => {
        this.loading = true;
        const file = reader.result.toString().split(',')[1];
        const model = { File: file };
        this.productUrl = { file: model.File };
        this.adminService
          .updateStoreImage(this.productUrl, this.routeId)
          .subscribe(
            img => { },
            error => {
              alert('An error occured, please try again shortly');
              this.loading = false;
            }
          );
      };
      setTimeout(() => {
        alert('Store profile picture uploaded successfully');
        this.getStoreImage();
        this.loading = false;
      }, 15000);
    }
  }

  async getStoreImage() {
    await this.adminService.getStoreImageById(this.routeId).subscribe(img => {
      if (img) {
        this.storeImage = 'data:image/png;base64,' + img.file;
      } else {
        return;
      }

    });
  }

  confirmOrder(id, i) {
    this.orderStatus = { orderStatus: 'Sold' };
    this.marketService
      .updateOrders(this.orderStatus, id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.confirmLoad = true;
        this.getDashboardData();
        this.pendingOrderCustomer.splice(i, 1);
        this.pendingOrderProducts.splice(i, 1);
        setTimeout(() => {
          if (this.getDashboardData) {

          }
        }, 5000);
      },
        error => {
          alert('An error occured, please try again later');
        });

  }
  storeWalletValue() {
    this.getBalance();
    this.walletBalance = this.localStorage.getItem('walletBalance');
    const walletAmount = this.amount / 100;
    if (this.paymentStatus === 'success' && this.walletBalance !== null) {
      const oldAmount = Number(this.walletBalance);
      const newAmount = Number(this.amount / 100);
      this.updatedBalance = oldAmount + newAmount;
      console.log('updated balance', this.updatedBalance);
      const wallet = { balance: this.updatedBalance };
      this.adminService.updateWallet(wallet, this.routeId).subscribe(wall => {
        this.getBalance();
      });

    } else {
      const wallet = { balance: walletAmount, storeId: this.routeId };
      this.adminService.fundWallet(wallet).subscribe(wall => {
        this.getBalance();
      });
    }
  }
  async getBalance() {
    // localStorage.removeItem('walletBalance');
    await this.adminService
      .getStoresById(this.routeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        if (data.wallet == null) {
          this.walletBal = null;
          return;
        } else {
          this.walletBal = data.wallet.balance;
          JSON.stringify(this.localStorage.setItem('walletBalance', this.walletBal));
          this.balance = this.walletBal;
        }
      });
  }
  postProductAds() {
    const adsPrice = JSON.parse(this.localStorage.getItem('promoPrice'));
    this.balance = JSON.parse(this.localStorage.getItem('walletBalance'));
    if (this.balance < adsPrice) {
      alert('You do not have sufficient funds to promote this product');
    } else {
      const newBalance = this.balance - adsPrice;
      this.updatedBalance = newBalance;
      const wallet = { balance: this.updatedBalance };

      const formModel = this.promoteProductForm.value;
      this.adminService
        .postProductAds(formModel)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(ads => {
          this.adminService
            .updateWallet(wallet, this.routeId)
            .subscribe(wall => {
              this.getBalance();
              this.showSelectedProductPromoPackage = false;
              alert('Promotion Activated');
            });
        });
    }
  }
  postDiscountAds() {
    const adsPrice = JSON.parse(this.localStorage.getItem('promoPrice'));
    this.balance = JSON.parse(this.localStorage.getItem('walletBalance'));
    if (this.balance < adsPrice) {
      alert('You do not have sufficient funds to promote this product');
    } else {
      const newBalance = this.balance - adsPrice;
      this.updatedBalance = newBalance;
      const wallet = { balance: this.updatedBalance };

      const formModel = this.promoteDiscountForm.value;
      this.adminService
        .postProductAds(formModel)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(ads => {
          this.adminService
            .updateWallet(wallet, this.routeId)
            .subscribe(wall => {
              this.getBalance();
              this.showSelectedProductPromoPackage = false;
              alert('Discount Promotion Activated');
            });
        });
    }
  }
  postStoreAds() {
    const adsPrice = JSON.parse(this.localStorage.getItem('promoPrice'));
    this.balance = JSON.parse(this.localStorage.getItem('walletBalance'));
    if (this.balance < adsPrice) {
      alert('You do not have sufficient funds to promote this product');
    } else {
      const newBalance = this.balance - adsPrice;
      this.updatedBalance = newBalance;
      const wallet = { balance: this.updatedBalance };

      const formModel = this.promoteStoreForm.value;
      this.adminService
        .postStoreAds(formModel)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(ads => {
          this.adminService
            .updateWallet(wallet, this.routeId)
            .subscribe(wall => {
              this.getBalance();
              this.showSelectedPromoPackage = false;
              alert('Store Promotion Activated');
            });
        });
    }
  }
  deleteCustomer(id) {
    console.log('customer deleted', id);
  }

  getPromoPackages() {
    this.adminService
      .getPromotionPackages()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.promotionPackage = res;
        console.log('promo package', res);
      });
  }
  getSubmitStatus() {
    this.storeService.submitted.subscribe(data => {
      const submitted = data;
      if (submitted === true) {
        this.getDashboardData();
      }
    });
  }
  getSelectedPromoPackage(event) {
    const selectedPromoPackage = event.target.value;
    this.adminService
      .getPromotionPackagesById(selectedPromoPackage)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        data => {
          this.selectedPackage = data;
          const promoPrice = this.selectedPackage.price;
          JSON.stringify(this.localStorage.setItem('promoPrice', promoPrice));
          const promoId = this.selectedPackage.promotionId;
          const packageDuration = this.selectedPackage.duration;
          const adsStartDate = new Date();
          this.adsStartDate = new Date().toDateString();
          const endadsDate = adsStartDate.setDate(
            adsStartDate.getDate() + packageDuration
          );
          this.endAdsDate = new Date(endadsDate).toDateString();
          this.showSelectedPackage = true;
          this.promoteStoreForm.patchValue({
            startDate: this.adsStartDate,
            endDate: this.endAdsDate,
            promotionId: promoId
          });
        },
        error => {
          this.showSelectedPackage = false;
          alert(
            'An error occured while trying to fetch the selected package. Please try again shortly'
          );
        }
      );
  }

  getSelectedPromoProductPackage(event) {
    const selectedProductPromoPackage = event.target.value;
    this.adminService
      .getPromotionPackagesById(selectedProductPromoPackage)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        data => {
          this.selectedPackage = data;
          const promoPrice = this.selectedPackage.price;
          JSON.stringify(this.localStorage.setItem('promoPrice', promoPrice));
          const promoId = this.selectedPackage.promotionId;
          const packageDuration = this.selectedPackage.duration;
          const adsStartDate = new Date();
          this.adsStartDate = new Date().toDateString();
          const endadsDate = adsStartDate.setDate(
            adsStartDate.getDate() + packageDuration
          );
          this.endAdsDate = new Date(endadsDate).toDateString();
          this.showSelectedProductPromoPackage = true;
          this.promoteProductForm.patchValue({
            startDate: this.adsStartDate,
            endDate: this.endAdsDate,
            promotionId: promoId
          });
        },
        error => {
          this.showSelectedProductPromoPackage = false;
          alert(
            'An error occured while trying to fetch the selected package. Please try again shortly'
          );
        }
      );
  }
  getselectedProduct(event) {
    this.isSelectedProduct = true;
    const selectedProductId = event.target.value;
    this.adminService
      .getProductById(selectedProductId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.promoProduct = data;
      });
  }
  getDiscountPromoProduct(event) {
    this.isSelectedDiscount = true;
    const selectedDiscountId = event.target.value;
    this.adminService
      .getProductById(selectedDiscountId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.promoDiscount = data;
      });
  }
  getSelectedPromoDiscountPackage(event) {
    const selectedDiscountPromoPackage = event.target.value;
    this.adminService
      .getPromotionPackagesById(selectedDiscountPromoPackage)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        data => {
          this.selectedPackage = data;
          const promoPrice = this.selectedPackage.price;
          JSON.stringify(this.localStorage.setItem('promoPrice', promoPrice));
          const promoId = this.selectedPackage.promotionId;
          const packageDuration = this.selectedPackage.duration;
          const adsStartDate = new Date();
          this.adsStartDate = new Date().toDateString();
          const endadsDate = adsStartDate.setDate(
            adsStartDate.getDate() + packageDuration
          );
          this.endAdsDate = new Date(endadsDate).toDateString();
          this.showSelectedDiscountPromoPackage = true;
          this.promoteDiscountForm.patchValue({
            startDate: this.adsStartDate,
            endDate: this.endAdsDate,
            promotionId: promoId
          });
        },
        error => {
          this.showSelectedDiscountPromoPackage = false;
          alert(
            'An error occured while trying to fetch the selected package. Please try again shortly'
          );
        }
      );
  }
  promoteStore() {
    this.showStoreAds = !this.showStoreAds;
    this.showProductAds = false;
    this.showDiscountAds = false;
  }
  promoteProduct() {
    this.showStoreAds = false;
    this.showProductAds = !this.showProductAds;
    this.showDiscountAds = false;
  }
  promoteDiscount() {
    this.showStoreAds = false;
    this.showProductAds = false;
    this.showDiscountAds = !this.showDiscountAds;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
