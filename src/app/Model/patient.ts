import { User } from './user';

export class Patient {
  patientId: string;
  userId: string;
  doctorId: string;
  user: User;
}
