import { Products } from "./products";

export class Stores {
  StoreId: any;
  Name: string;
  Description: string;
  Confirmed: boolean;
  Products: Products[];
  StoreUrl: string;
  FirstName: string;
  LastName: string;
  Address: string;
  Email: string;
  StoreAds: StoreAds;
  Wallet: Wallet;
  StoreType: string;

}


export class StoreAds {
  Id: any;
  StoreId: any;
  Status: string;
  StartDate: string;
  EndDate: string;
}


export class Wallet {
  Id: any;
  StoreId: any;
  balance: any
}

// export class Merchant {
//   FirstName: string;
//   LastName: string;
//   Address: string;
//   Email: string;
// }

