import { Stores } from "./stores";

export class Review {
  Id: string;
  Comment: string;
  UserRate: number;
  StoreId: string;
  CommentedOn: string;
  CommentBy: string;
  Store: Stores;
}
