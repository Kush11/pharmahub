export enum Role {
  User = 'User',
  Admin = 'Admin',
  Customer = 'Customer',
  Merchant = 'Merchant',
  Doctor = 'Doctor'
}
