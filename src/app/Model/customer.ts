export class Customer {
  Id: any;
  UserName: string;
  StoreId: string;
  StoreName: string;
  Address: string;
  PhoneNumber: string;
}
