import { Stores } from './stores';
import { UserPaymentToken } from './payment';

export class User {

  userId: any;
  firstName: string;
  lastName: string;
  password: string;
  phoneNumber: string;
  address: string;
  token?: string;
  role: string;
  stores: Stores;
  connectionId: string;
  email: string;
  connectionPass: string;
  imageUrl: string;
  gender: string;
  age: string;
  userPaymentData: UserPaymentToken[];

}
