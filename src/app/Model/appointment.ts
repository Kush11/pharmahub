import { User } from './user';
import { Prescription } from './prescription';

export class Appointment {
  sessionId: string;
  patientId: string;
  doctorId: string;
  sessionStartDateTime: string;
  sessionEndDateTime: string;
  status: string;
  sessionCode: string;
  diagnosis: string;
  isPaymentDone: boolean;
  patient: User;
  prescription: Prescription[];
}
