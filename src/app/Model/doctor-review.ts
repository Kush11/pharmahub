import { Doctor } from "./doctor";

export class DoctorReview {
  ReviewId: string;
  Comment: string;
  UserRate: number;
  DoctorId: string;
  CommentedOn: string;
  CommentBy: string;
  DoctorDetails: Doctor;
}