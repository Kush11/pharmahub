import { User } from './user';

export class Doctor {
  doctorId: string;
  userId: string;
  specializationId: string;
  licenseUrl: string;
  startTime: string;
  endTime: string;
  rating: string;
  status: string;
  isAvailable: boolean;
  workingDays: string;
  user: User;
  paymentAccountId: string;
}
