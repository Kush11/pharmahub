export class Prescription {
  prescriptionId: string;
  sessionId: string;
  comment: string;
}